/**
 * @file CoreDefine.h
 * @brief Various constants of GWBASE are defined. Modification not recommended.
*/

#ifndef GWBASE_CORE_DEFINE_H
#define GWBASE_CORE_DEFINE_H

#define __LOG_LEVEL_OFF           0
#define __LOG_LEVEL_ERROR         1
#define __LOG_LEVEL_WARNING       2
#define __LOG_LEVEL_INFORMATION   3
#define __LOG_LEVEL_GENERAL       4
#define __LOG_LEVEL_DEBUGGING     5
#define __LOG_LEVEL_ALL           10


namespace GWBASE
{
    #define IPC_BUFF_SIZE 512

    typedef struct _IPCMsgBuff
    {
        long msgType;
        uint32_t callFunc;
        int32_t callbackManagerKey;
        int32_t callbackFunc;
        int64_t arg1;
        int64_t arg2;
        char data1[IPC_BUFF_SIZE];
        char data2[IPC_BUFF_SIZE];
        char data3[IPC_BUFF_SIZE];
        char data4[IPC_BUFF_SIZE];
    }IPCMsgBuff;

    enum class GWBaseErrorType {
        GW_NOERROR = 0,
        GW_UNKOWN_ERROR = -1,
        GW_OUT_OF_LENGTH = -2,
        GW_NULLPTR = -3,
        GW_INVALID_PARAM = -4,
        GW_FILE_OPEN_FAIL = -5,
    };

    namespace GWMESSAGE {
        class GWMessage;

        typedef struct _MessageObject {
            _MessageObject();
            void* vPointer1;
            void* vPointer2;
            void* vPointer3;
            void* vPointer4;
            int32_t callbackManagerKey;
            int32_t callbackFunc;
        }GWMessageObj;
    } // GWMESSAGE

    class GWSemaphore;
    class GWMutex;

    extern std::string processName;

    #define MESSAGE_BUFF_SIZE 100
} // namespace GWBASE


#endif // GWBASE_CORE_DEFINE_H