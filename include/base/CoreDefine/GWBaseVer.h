#ifndef GWBASE_VERSION_H
#define GWBASE_VERSION_H

// major version. There are changes that are not compatible with the previous version.
#define GW_BASE_VERSION_MAJOR       1
// minor version. Compatibility with the previous version is supported, but there are distinct changes.
#define GW_BASE_VERSION_MINOR       4
// patch version. Features added or removed.
#define GW_BASE_VERSION_PATCH       1
// build version. Fix bugs or make changes that don't affect functionality.
#define GW_BASE_VERSION_BUILD       0

#define GW_TOSTR(str)                                   #str

#define GW_TOSTRVER(major, minor, patch, build)         GW_TOSTR(major.minor.patch.build)

#define GW_TOSTRDATE(year, month, day, h, m)            GW_TOSTR(year.month.day::h:m)

#define __GW_BASE_VERSION                               GW_TOSTRVER(GW_BASE_VERSION_MAJOR, GW_BASE_VERSION_MINOR, GW_BASE_VERSION_PATCH, GW_BASE_VERSION_BUILD)
#define GW_BASE_VERSION                                 __GW_BASE_VERSION

#define GW_BASE_MODIFIED_YEAR       2022
#define GW_BASE_MODIFIED_MONTH      09
#define GW_BASE_MODIFIED_DAY        29
#define GW_BASE_MODIFIED_HOUR       18
#define GW_BASE_MODIFIED_MINUTE     10

#define __GW_BASE_LAST_MODIFIED_DATE                    GW_TOSTRDATE(GW_BASE_MODIFIED_YEAR, GW_BASE_MODIFIED_MONTH, GW_BASE_MODIFIED_DAY, GW_BASE_MODIFIED_HOUR, GW_BASE_MODIFIED_MINUTE)
#define GW_BASE_LAST_MODIFIED_DATE                      __GW_BASE_LAST_MODIFIED_DATE

#endif // GWBASE_VERSION_H