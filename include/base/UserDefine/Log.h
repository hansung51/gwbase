#ifndef GWBASE_LOG_H
#define GWBASE_LOG_H

#include "../CoreDefine/CoreDefine.h"

// Path to save log files
#define LOG_PATH "/home/gunwoo/project/data/log/"
// name to save log files
#define LOG_NAME "Log.txt"


#define LOG_LEVEL_OFF                   __LOG_LEVEL_OFF
#define LOG_LEVEL_ERROR                 __LOG_LEVEL_ERROR
#define LOG_LEVEL_WARNING               __LOG_LEVEL_WARNING
#define LOG_LEVEL_INFORMATION           __LOG_LEVEL_INFORMATION
#define LOG_LEVEL_GENERAL               __LOG_LEVEL_GENERAL
#define LOG_LEVEL_DEBUGGING             __LOG_LEVEL_DEBUGGING
#define LOG_LEVEL_ALL                   __LOG_LEVEL_ALL

/**
*   @brief LOG_LEVEL
*       Specifies the level of log output to the console.
*       All levels below the set level are output to the console.
*       The file stores all levels of logs regardless of settings.
*/
#define LOG_LEVEL               LOG_LEVEL_ALL

namespace GWBASE {
    namespace GWLOG {
        // Maximum size of log file will have 20MB. If you want, define value yourself.
        const static int32_t LOG_FILE_MAX_SIZE  = 20 * 1000 * 1000;
        // Maximum number of log file will have 30. If you want, define value yourself.
        const static int32_t LOG_FILE_MAX_COUNT = 30;
    };
};

#endif // GWBASE_LOG_H