#include "../../include/base/GWBase.h"


GWBASE::GWFILE::GWFile::GWFile() {
    mInitSet = false;
}

bool GWBASE::GWFILE::GWFile::InitFile() {
    const char* iName = "File";

    mInitSet = true;
    return true;
}

int32_t GWBASE::GWFILE::GWFile::GetFileSize(const char* pPath, const char* pName) {
    FILE* iFp = NULL;
    int32_t iResult;

    if (pPath == nullptr) {
        return static_cast<int>(GWBaseErrorType::GW_NULLPTR);
    }
    if (pName == nullptr) {
        return static_cast<int>(GWBaseErrorType::GW_NULLPTR);
    }
    if (strlen(pPath) <= 0) {
        return static_cast<int>(GWBaseErrorType::GW_INVALID_PARAM);
    }
    if (strlen(pName) <= 0) {
        return static_cast<int>(GWBaseErrorType::GW_INVALID_PARAM);
    }

    OpenFile(&iFp, pPath, pName, "rb");
    if (iFp == NULL) {
        return static_cast<int>(GWBaseErrorType::GW_FILE_OPEN_FAIL);
    }

    fseek(iFp, 0, SEEK_END);
    iResult = ftell(iFp);

    if (iFp != NULL) {
        fclose(iFp);
    }
    return iResult;
}

int32_t GWBASE::GWFILE::GWFile::GetFileSize(const char* pPath, const std::string  pName) {
    return GetFileSize(pPath, pName.c_str());
}

int32_t GWBASE::GWFILE::GWFile::GetFileSize(const char* pPath, const std::string* pName) {
    return GetFileSize(pPath, pName->c_str());
}

int32_t GWBASE::GWFILE::GWFile::GetFileSize(const std::string  pPath, const char* pName) {
    return GetFileSize(pPath.c_str(), pName);
}

int32_t GWBASE::GWFILE::GWFile::GetFileSize(const std::string* pPath, const char* pName) {
    return GetFileSize(pPath->c_str(), pName);
}

int32_t GWBASE::GWFILE::GWFile::GetFileSize(const std::string  pPath, std::string  pName) {
    return GetFileSize(pPath.c_str(), pName.c_str());
}

int32_t GWBASE::GWFILE::GWFile::GetFileSize(const std::string  pPath, std::string* pName) {
    return GetFileSize(pPath.c_str(), pName->c_str());
}

int32_t GWBASE::GWFILE::GWFile::GetFileSize(const std::string* pPath, std::string  pName) {
    return GetFileSize(pPath->c_str(), pName.c_str());
}

int32_t GWBASE::GWFILE::GWFile::GetFileSize(const std::string* pPath, std::string* pName) {
    return GetFileSize(pPath->c_str(), pName->c_str());
}

void* GWBASE::GWFILE::GWFile::ShiftFileName(void* pArg) {
    LogShiftArg* iArg = static_cast<LogShiftArg*>(pArg);
    std::string iFileOrig;
    std::string iFileShift;

    if (iArg->mFileClass->mInitSet == false) {
        return nullptr;
    }

    if (iArg->mFileClass == nullptr) {
        return nullptr;
    }
    if (iArg->mPath == nullptr) {
        return nullptr;
    }
    if (iArg->mFileName == nullptr) {
        return nullptr;
    }
    if (iArg->mLogMtx == nullptr) {
        return nullptr;
    }
    if (strlen(iArg->mPath) <= 0) {
        return nullptr;
    }
    if (iArg->mFileName->size() <= 0) {
        return nullptr;
    }
    while (true) {
        iArg->mSigMtx->Lock();
        iArg->mSigMtx->CondWait();
        if (iArg->mFileClass->GetFileSize(iArg->mPath, iArg->mFileName->c_str()) >= GWBASE::GWLOG::LOG_FILE_MAX_SIZE) {
            for (int32_t i = (iArg->mCount - 1); i >= 0; i--) {
                if (i == 0) {
                    iArg->mLogMtx->Lock();
                }
                iArg->mFileClass->MakeShiftName(&iFileOrig, iArg->mPath, iArg->mFileName->c_str(), i);
                iArg->mFileClass->MakeShiftName(&iFileShift, iArg->mPath, iArg->mFileName->c_str(), i + 1);
                remove(iFileShift.c_str());
                rename(iFileOrig.c_str(), iFileShift.c_str());
                if (i == 0) {
                    iArg->mLogMtx->UnLock();
                }
            }
        }
        iArg->mSigMtx->UnLock();
    }

    free(pArg);

    return nullptr;
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::ShiftFileName(const char* pPath, const char* pName, const uint32_t pShiftCount) {
    std::string iFileOrig;
    std::string iFileShift;

    if (pPath == nullptr) {
        return GWBaseErrorType::GW_NULLPTR;
    }
    if (pName == nullptr) {
        return GWBaseErrorType::GW_NULLPTR;
    }
    if (strlen(pPath) <= 0) {
        return GWBaseErrorType::GW_INVALID_PARAM;
    }
    if (strlen(pName) <= 0) {
        return GWBaseErrorType::GW_INVALID_PARAM;
    }

    for (int32_t i = (pShiftCount - 1); i >= 0; i--) {
        MakeShiftName(&iFileOrig, pPath, pName, i);
        MakeShiftName(&iFileShift, pPath, pName, i + 1);
        remove(iFileShift.c_str());
        rename(iFileOrig.c_str(), iFileShift.c_str());
    }

    return GWBaseErrorType::GW_NOERROR;
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::ShiftFileName(const char* pPath, const std::string  pName, const uint32_t pShiftCount) {
    return ShiftFileName(pPath, pName.c_str(), pShiftCount);
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::ShiftFileName(const char* pPath, const std::string* pName, const uint32_t pShiftCount) {
    return ShiftFileName(pPath, pName->c_str(), pShiftCount);
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::ShiftFileName(const std::string  pPath, const char* pName, const uint32_t pShiftCount) {
    return ShiftFileName(pPath.c_str(), pName, pShiftCount);
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::ShiftFileName(const std::string* pPath, const char* pName, const uint32_t pShiftCount) {
    return ShiftFileName(pPath->c_str(), pName, pShiftCount);
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::ShiftFileName(const std::string  pPath, const std::string  pName, const uint32_t pShiftCount) {
    return ShiftFileName(pPath.c_str(), pName.c_str(), pShiftCount);
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::ShiftFileName(const std::string  pPath, const std::string* pName, const uint32_t pShiftCount) {
    return ShiftFileName(pPath.c_str(), pName->c_str(), pShiftCount);
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::ShiftFileName(const std::string* pPath, const std::string  pName, const uint32_t pShiftCount) {
    return ShiftFileName(pPath->c_str(), pName.c_str(), pShiftCount);
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::ShiftFileName(const std::string* pPath, const std::string* pName, const uint32_t pShiftCount) {
    return ShiftFileName(pPath->c_str(), pName->c_str(), pShiftCount);
}

//https://m31phy.tistory.com/143 mkdirs func
GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::MakeDirs(const char* pPath) {
    const int32_t iBuffLen = 1024;
    char iBuff[iBuffLen];
    char* iStrPos = iBuff;

    if (strlen(pPath) >= iBuffLen) {
        return GWBaseErrorType::GW_OUT_OF_LENGTH;
    }

    memset(iBuff, 0, sizeof(iBuff));
    strcpy(iBuff, pPath);

    while(*iStrPos != '\0') {
        if ('/' == *iStrPos) {
            *iStrPos = '\0';
            mkdir(iBuff, ALLPERMS);
            *iStrPos = '/';
        }
        iStrPos++;
    }
    return GWBaseErrorType::GW_NOERROR;
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::MakeDirs(const std::string  pPath) {
    return MakeDirs(pPath.c_str());
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::MakeDirs(const std::string* pPath) {
    return MakeDirs(pPath->c_str());
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::OpenFile(FILE** pFile, const char* pPath, const char* pName, const char* pMode) {
    std::string iFile;
    if (pPath == nullptr) {
        return GWBaseErrorType::GW_NULLPTR;
    }
    if (pName == nullptr) {
        return GWBaseErrorType::GW_NULLPTR;
    }
    if (pMode == nullptr) {
        return GWBaseErrorType::GW_NULLPTR;
    }
    if (strlen(pPath) <= 0) {
        return GWBaseErrorType::GW_INVALID_PARAM;
    }
    if (strlen(pName) <= 0) {
        return GWBaseErrorType::GW_INVALID_PARAM;
    }
    if (strlen(pMode) <= 0) {
        return GWBaseErrorType::GW_INVALID_PARAM;
    }

    iFile.clear();
    iFile = pPath;
    iFile += pName;

    *pFile = fopen(iFile.c_str(), pMode);
    if (*pFile == NULL) {
        //printf("error: %d\n", errno);
        return GWBaseErrorType::GW_FILE_OPEN_FAIL;
    }

    return GWBaseErrorType::GW_NOERROR;
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::OpenFile(FILE** pFile, const char* pPath, const std::string  pName, const char* pMode) {
    return OpenFile(pFile, pPath, pName.c_str(), pMode);
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::OpenFile(FILE** pFile, const char* pPath, const std::string* pName, const char* pMode) {
    return OpenFile(pFile, pPath, pName->c_str(), pMode);
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::OpenFile(FILE** pFile, const std::string* pPath, const char* pName, const char* pMode) {
    return OpenFile(pFile, pPath->c_str(), pName, pMode);
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::OpenFile(FILE** pFile, const std::string  pPath, const char* pName, const char* pMode) {
    return OpenFile(pFile, pPath.c_str(), pName, pMode);
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::OpenFile(FILE** pFile, const std::string  pPath, const std::string  pName, const char* pMode) {
    return OpenFile(pFile, pPath.c_str(), pName.c_str(), pMode);
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::OpenFile(FILE** pFile, const std::string  pPath, const std::string* pName, const char* pMode) {
    return OpenFile(pFile, pPath.c_str(), pName->c_str(), pMode);
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::OpenFile(FILE** pFile, const std::string* pPath, const std::string  pName, const char* pMode) {
    return OpenFile(pFile, pPath->c_str(), pName.c_str(), pMode);
}

GWBASE::GWBaseErrorType GWBASE::GWFILE::GWFile::OpenFile(FILE** pFile, const std::string* pPath, const std::string* pName, const char* pMode) {
    return OpenFile(pFile, pPath->c_str(), pName->c_str(), pMode);
}

void GWBASE::GWFILE::GWFile::MakeShiftName(std::string* pFileName, const char* pPath, const char* pName, const int32_t pNum) {
    std::string iFileName;
    std::string iFileExtension;
    char iNum[11];

    if (pFileName == nullptr || pPath == nullptr || pName == nullptr) {
        return;
    }

    memset(iNum, 0, sizeof(iNum));
    pFileName->clear();
    iFileName.clear();
    iFileExtension.clear();
    iFileName = pName;

    for (int32_t i = strlen(pName); i >= 0; i--) {
        if (*(pName + i) == '.') {
            iFileExtension = iFileName.substr(i + 1);
            iFileName = iFileName.substr(0, i);
            break;
        }
    }

    sprintf(iNum, "%d", pNum);
    *pFileName = pPath;
    *pFileName += iFileName.c_str();
    if (pNum != 0) {
        *pFileName += "(";
        *pFileName += iNum;
        *pFileName += ")";
    }
    if (iFileExtension.size() != 0) {
        *pFileName += ".";
        *pFileName += iFileExtension.c_str();
    }

    return;
}