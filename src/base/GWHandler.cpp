#include "../../include/base/GWBase.h"

GWBASE::GWHANDLER::GWHandler::GWHandler() {}

void GWBASE::GWHANDLER::GWHandler::SetLooper(GWLOOPER::GWLooper* pLooper) {
    mLooper = pLooper;
}

GWBASE::GWMESSAGE::GWMessage* GWBASE::GWHANDLER::GWHandler::ObtainMsg() {
    mMsgSetMutex.lock();
    GWMESSAGE::GWMessage* iResult = NULL;
    if (MESSAGE_BUFF_SIZE != mLooper->GetMsgBufSize()) {
        // 해당 반복문은 이후 search 알고리즘 만들어 대체하여야 함. 현재로는 매우 느리고 또한 구상중인 기능에 알맞지 않음.
        // 개선 시 해당 부분과 유기적으로 연결되어 있는 부분들 모두 고려해야 함.
        // 기능 개선할 때, 연결되어 동작되는 다른 기능 함수들을 최대한 분리시키는 방향으로 개발해야 함.
        // 현재 컨셉에서 handler, looper, message queue가 서로 유기적으로 얽힌 부분이 가장 큰 문제점. 코드 재사용도 어렵고 유지보수도 어려움.
        // 개선 시, 각 기능 동작별로 최대한 의존성을 낮추어 단위 함수로 구현해야 함.
        //
        // 개선방안 1.
        // * 현재 구현된 container 버퍼와 비어있는 container index를 관리하는 queue를 생성, 비어있는 index를 queue 에서 pop하여 가져 옴.
        // * 이벤트 메세지 전달 후 queue에 자기 자신의 index push.
        // * 딜레이 메세지또한 같은 방식으로 구현. (index push 함수 만들어서 딜레이 메세지에서도 재사용)
        // *
        // * 문제점: 큐 push pop마다 new del을 사용하게 되어서 느림. 일반적인 상황에선 오히려 지금 while 동작이 빠를 것 같음. 에러 리턴시 메모리 릭 우려.
        // * 필수적으로, 기능은 빠르고 또한 안전해야 함.
        // *
        // * 대체 방안 생각 필요. (알맞은 search 알고리즘 찾기!)
        // * https://bright-effect.com/32 추천받음. 시간될 떄 컨셉 읽어보고 모델 검증해보기!
        while (mLooper->GetMsgInContainer()->GetIsUse() == true) {
            mLooper->IncreaseMsgIndex();
        }
        iResult = mLooper->GetMsgInContainer();
        iResult->InitMsg(this, mLooper->GetMsgIndex());
        mLooper->IncreaseMsgBufSize();
    }

    mMsgSetMutex.unlock();
    return iResult;
}

void GWBASE::GWHANDLER::GWHandler::HandleMessage(GWMESSAGE::GWMessage* msg) {
    LOG_W("GWHandler::handleMessage() some error! this handler is in the GWHandler.cpp!");
    GWBASE::GWMESSAGE::GWMessageObj iObj = msg->GetObj();
    int32_t iFunc = msg->GetCallFunc();

    switch (iFunc) {
        default :
            break;
    }
}
