#include "../../include/base/GWBase.h"

//https://kldp.org/node/107186

GWBASE::GWMutex::GWMutex() {
    mInitSet = false;
}

GWBASE::GWBaseErrorType GWBASE::GWMutex::InitMutex(const char* pMtxName) {
    int32_t fd = 0;
    int32_t shm_size = 0;
    int32_t ret = 0;
    std::string iName = processName;

    if (pMtxName == nullptr) {
        return GWBASE::GWBaseErrorType::GW_NULLPTR;
    }
    if (strlen(pMtxName) == 0) {
        return GWBASE::GWBaseErrorType::GW_NULLPTR;
    }

    iName += "_";
    iName += pMtxName;

    fd = shm_open(iName.c_str(), O_RDWR|O_CREAT, S_IRUSR|S_IWUSR|S_IROTH);
    if (fd == -1) {
        printf("shm_open() error\n");
        return GWBASE::GWBaseErrorType::GW_FILE_OPEN_FAIL;
    }
    shm_size = sizeof(ThreadStruct);
    ret = ftruncate(fd, shm_size);
    if (ret == -1) {
        printf("ftruncate() failed. errno=%d\n", errno);
        return GWBASE::GWBaseErrorType::GW_UNKOWN_ERROR;
    }

    mTm = (ThreadStruct*)mmap(NULL, shm_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    if (mTm == MAP_FAILED) {
        printf("mmap fail\n");
        return GWBASE::GWBaseErrorType::GW_UNKOWN_ERROR;
    }

    pthread_mutexattr_init(&mLogMutexAtt);
    ret = pthread_mutexattr_setpshared(&mLogMutexAtt, PTHREAD_PROCESS_SHARED);
    if (ret) {
        printf("pthread_mutexattr_setpshared fail\n");
        return GWBASE::GWBaseErrorType::GW_UNKOWN_ERROR;
    }
    pthread_mutex_init(&mTm->mtx, &mLogMutexAtt);

    pthread_condattr_init(&mLogCondAtt);
    ret = pthread_condattr_setpshared(&mLogCondAtt, PTHREAD_PROCESS_SHARED);
    if (ret) {
        printf("pthread_condattr_setpshared fail\n");
        return GWBASE::GWBaseErrorType::GW_UNKOWN_ERROR;
    }
    pthread_cond_init(&mTm->cond, &mLogCondAtt);

    mInitSet = true;
    return GWBASE::GWBaseErrorType::GW_NOERROR;
}

bool GWBASE::GWMutex::Lock() {
    int32_t ret;
    if (mInitSet == true) {
        ret = pthread_mutex_lock(&mTm->mtx);
        if (ret == 0) {
            return true;
        }
    }
    return false;
}

bool GWBASE::GWMutex::UnLock() {
    int32_t ret;
    if (mInitSet == true) {
        ret = pthread_mutex_unlock(&mTm->mtx);
        if (ret == 0) {
            return true;
        }
    }
    return false;
}

bool GWBASE::GWMutex::CondWait() {
    int32_t ret;
    if (mInitSet == true) {
        ret = pthread_cond_wait(&mTm->cond, &mTm->mtx);
        if (ret == 0) {
            return true;
        }
    }
    return false;
}


bool GWBASE::GWMutex::CondSig() {
    int32_t ret;
    if (mInitSet == true) {
        ret = pthread_cond_signal(&mTm->cond);
        if (ret == 0) {
            return true;
        }
    }
    return false;
}