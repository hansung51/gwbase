#include "../../include/base/GWBase.h"

GWBASE::GWSemaphore::GWSemaphore(const char* pSemaName, bool pFlag , uint32_t pVal, time_t pTimeOutSec) {
    mVal = pVal;
    mTimeOut = pTimeOutSec;
    mSemaName.clear();
    mSemaName = "/";
    if (pFlag == false) {
        mSemaName += processName.c_str();
        mSemaName += "_";
    }
    if (pSemaName != nullptr) {
        mSemaName += pSemaName;
    }
    mSem = sem_open(mSemaName.c_str(), O_CREAT, 0666, mVal);

    sem_init(mSem, 0, mVal);
    printf("mSem: %p\n", mSem);
    if (CheckProcessDupl(processName) == false) {
        sem_unlink(mSemaName.c_str());
        mSem = sem_open(mSemaName.c_str(), O_CREAT, 0666, mVal);
        printf("O_CREAT\n");
    } else {
        mSem = sem_open(mSemaName.c_str(), O_EXCL);
        printf("O_EXCL\n");
    }
    printf("mSem: %p, mSemaName: %s\n", mSem, mSemaName.c_str());
    if (mSem == NULL) {
        printf("error : %d\n", errno);
    }

    return;
}

bool GWBASE::GWSemaphore::Lock() {
    printf("lock++\n");
    if (mSem) {
        printf("mSem\n");
        clock_gettime(CLOCK_REALTIME, &mTm);
        mTm.tv_sec = mTimeOut;

        if (!sem_timedwait(mSem, &mTm)) {
            printf("sem_timedwait\n");
            return true;
        }
    }
    return false;
}

bool GWBASE::GWSemaphore::UnLock() {
    printf("unlock++\n");
    if (!sem_post(mSem)) {
        printf("sem_post\n");
        return true;
    }

    return false;
}

bool GWBASE::GWSemaphore::CheckProcessDupl(const std::string& pSemaName) {
    FILE *fp = nullptr;
    std::string pid;
    char cpid[64] = {0, };
    uint32_t run_cnt = 0;

    std::ostringstream oss;
    oss << "ps -ef | grep " << pSemaName << " | grep -v grep | awk -F' ' '{print $2}'";
    if (nullptr != (fp = popen(oss.str().c_str(), "r"))) {
        while(fgets(cpid , 64, fp)) {
            run_cnt++;
        }
        pclose(fp);
    }

    return (run_cnt >= 2);
}