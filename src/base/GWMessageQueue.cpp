#include "../../include/base/GWBase.h"

GWBASE::GWMESSAGE::GWMessageObj::_MessageObject()  {
    vPointer1 = nullptr;
    vPointer2 = nullptr;
    vPointer3 = nullptr;
    vPointer4 = nullptr;
}

GWBASE::GWMESSAGE::GWMessage::GWMessage() {}

GWBASE::GWMESSAGE::GWMessage::GWMessage(GWLOOPER::GWLooper* pLooper) {
    mLooper = pLooper;
}

GWBASE::GWMESSAGE::GWMessage::GWMessage(GWLOOPER::GWLooper* pLooper, GWBASE::GWMESSAGE::GWMessage* msg) {
    mHandler = msg->GetHandler();
    mIndex = msg->GetIndex();
    mIsUse = msg->GetIsUse();
    mCallFunc = msg->GetCallFunc();
    mArg1 = msg->GetArg1();
    mArg2 = msg->GetArg2();
    mObject = msg->GetObj();
    mLooper = pLooper;
}


void GWBASE::GWMESSAGE::GWMessage::InitMsg(GWBASE::GWHANDLER::GWHandler* pHandler, uint32_t pIndex) {
    mHandler = pHandler;
    mIndex = pIndex;
    mIsUse = true;
    mCallFunc = 0;
    mArg1 = 0;
    mArg2 = 0;
    mObject.vPointer1 = nullptr;
    mObject.vPointer2 = nullptr;
    mObject.vPointer3 = nullptr;
    mObject.vPointer4 = nullptr;
}


GWBASE::GWMESSAGE::GWMessage* GWBASE::GWMESSAGE::GWMessage::SetMsg(uint32_t CallFunc) {
    mCallFunc = CallFunc;
    return this;
}

GWBASE::GWMESSAGE::GWMessage* GWBASE::GWMESSAGE::GWMessage::SetMsg(uint32_t CallFunc, uint64_t arg1, uint64_t arg2) {
    mCallFunc = CallFunc;
    mArg1 = arg1;
    mArg2 = arg2;
    return this;
}

GWBASE::GWMESSAGE::GWMessage* GWBASE::GWMESSAGE::GWMessage::SetMsg(uint32_t CallFunc, GWBASE::GWMESSAGE::GWMessageObj object) {
    mCallFunc = CallFunc;
    mObject = object;
    return this;
}


GWBASE::GWMESSAGE::GWMessage* GWBASE::GWMESSAGE::GWMessage::SetMsg(uint32_t CallFunc, GWBASE::GWMESSAGE::GWMessageObj object, uint64_t arg1, uint64_t arg2) {
    mCallFunc = CallFunc;
    mObject = object;
    mArg1 = arg1;
    mArg2 = arg2;
    return this;
}

bool GWBASE::GWMESSAGE::GWMessage::SendToTarget() {
    if (MESSAGE_BUFF_SIZE != mLooper->GetSize()) {
        mLooper->LooperLock();
        mLooper->SetIndexToBuff(mIndex);
        mLooper->IncreaseIndex();
        mLooper->IncreaseSize();
        mLooper->LooperWakeUp();
        mLooper->LooperUnlock();
        return true;
    } else {
        return false;
    }
}

GWBASE::GWHANDLER::GWHandler* GWBASE::GWMESSAGE::GWMessage::GetHandler() {
    return mHandler;
}

uint32_t GWBASE::GWMESSAGE::GWMessage::GetIndex() {
    return mIndex;
}

bool GWBASE::GWMESSAGE::GWMessage::GetIsUse() {
    return mIsUse;
}

uint32_t GWBASE::GWMESSAGE::GWMessage::GetCallFunc() {
    return mCallFunc;
}

uint64_t GWBASE::GWMESSAGE::GWMessage::GetArg1() {
    return mArg1;
}

uint64_t GWBASE::GWMESSAGE::GWMessage::GetArg2() {
    return mArg2;
}

GWBASE::GWMESSAGE::GWMessageObj GWBASE::GWMESSAGE::GWMessage::GetObj() {
    return mObject;
}


void GWBASE::GWMESSAGE::GWMessage::MsgClear() {
    mIsUse = false;
    mLooper->DecreaseMsgBufSize();
}

void GWBASE::GWMESSAGE::GWMessage::FreeObj() {
    if (mObject.vPointer1 != nullptr) {
        free(mObject.vPointer1);
        mObject.vPointer1 = nullptr;
    }
    if (mObject.vPointer2 != nullptr) {
        free(mObject.vPointer2);
        mObject.vPointer2 = nullptr;
    }
    if (mObject.vPointer3 != nullptr) {
        free(mObject.vPointer3);
        mObject.vPointer3 = nullptr;
    }
    if (mObject.vPointer4 != nullptr) {
        free(mObject.vPointer4);
        mObject.vPointer4 = nullptr;
    }
}
