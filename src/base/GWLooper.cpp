#include "../../include/base/GWBase.h"


void GWBASE::GWLOOPER::GWLooper::LooperInit() {
    for (uint32_t i = 0; i < MESSAGE_BUFF_SIZE; i++) {
        messageContainer[i] = new GWBASE::GWMESSAGE::GWMessage(this);
    }
    mMsgSetIndex = 0;
    mMsgBufSize = 0;
    memset(LooperBuff, 0, sizeof(LooperBuff));

    mLoopGetIndex = 0;
    mLoopBufSize = 0;
    mLoopSetIndex = 0;
    pthread_mutex_init(&mLooperSleepMtx, NULL);
}

void GWBASE::GWLOOPER::GWLooper::LooperRun() {
    uint32_t iIndex = 0;
    int32_t iResult = 0;
    while (true) {
        if (mLoopBufSize > 0) {
            iIndex = LooperBuff[mLoopGetIndex];
            mLoopGetIndex = (mLoopGetIndex + 1) % MESSAGE_BUFF_SIZE;
            mLoopBufSize--;
            GWBASE::GWMESSAGE::GWMessage* msg = messageContainer[iIndex];
            GWBASE::GWMESSAGE::GWMessage sendMsg = GWMESSAGE::GWMessage(this, msg);
            GWBASE::GWHANDLER::GWHandler* iHandler = sendMsg.GetHandler();
            if (iHandler == nullptr) {
                LOG_W("Message is not setting call handler. check to sample source code in how to use gwbase");
            } else {
                iHandler->HandleMessage(&sendMsg);
            }
            msg->FreeObj();
            msg->MsgClear();
            pthread_mutex_unlock(&mLooperSleepMtx);
        } else {
            iResult = pthread_mutex_lock(&mLooperSleepMtx);
        }
    }
}

void GWBASE::GWLOOPER::GWLooper::LooperLock() {
    mLoopSetMutex.lock();
}

void GWBASE::GWLOOPER::GWLooper::LooperUnlock() {
    mLoopSetMutex.unlock();
}

void GWBASE::GWLOOPER::GWLooper::LooperWakeUp() {
     pthread_mutex_unlock(&mLooperSleepMtx);
}

uint8_t GWBASE::GWLOOPER::GWLooper::GetSize() {
    return mLoopBufSize;
}

void GWBASE::GWLOOPER::GWLooper::IncreaseSize() {
    mLoopBufSize++;
}

void GWBASE::GWLOOPER::GWLooper::IncreaseIndex() {
    mLoopSetIndex = (mLoopSetIndex + 1) % MESSAGE_BUFF_SIZE;
}

void GWBASE::GWLOOPER::GWLooper::SetIndexToBuff(uint32_t pIndex) {
    LooperBuff[mLoopSetIndex] = pIndex;
}

GWBASE::GWMESSAGE::GWMessage* GWBASE::GWLOOPER::GWLooper::GetMsgInContainer() {
    return messageContainer[mMsgSetIndex];
}

uint8_t GWBASE::GWLOOPER::GWLooper::GetMsgBufSize() {
    return mMsgBufSize;
}

void GWBASE::GWLOOPER::GWLooper::IncreaseMsgBufSize() {
    mMsgBufSize++;
}

void GWBASE::GWLOOPER::GWLooper::DecreaseMsgBufSize() {
    mMsgBufSize--;
}

uint8_t GWBASE::GWLOOPER::GWLooper::GetMsgIndex() {
    return mMsgSetIndex;
}

void GWBASE::GWLOOPER::GWLooper::IncreaseMsgIndex() {
    mMsgSetIndex = (mMsgSetIndex + 1) % MESSAGE_BUFF_SIZE;
}