#include "../../include/base/GWBase.h"

//http://imarch.pe.kr/?p=1548
GWBASE::GWTHREAD::GWThread::GWThread () {}
GWBASE::GWTHREAD::GWThread::~GWThread () {}

void GWBASE::GWTHREAD::GWThread::ThreadRun() {
    return;
}

void* GWBASE::GWTHREAD::GWThread::_run(void* _pThis) {
    GWThread* pThis = (GWThread*)_pThis;
    pThis->ThreadRun();
    pthread_exit(NULL);
    return 0;
}

bool GWBASE::GWTHREAD::GWThread::ThreadStart() {
    int32_t iResult = 0;
    iResult = pthread_create(&mThread, NULL, GWBASE::GWTHREAD::GWThread::_run, (void*)this);
    if (iResult != 0) {
        return false;
    }

    return true;
}

pthread_t* GWBASE::GWTHREAD::GWThread::ThreadStart(void* (*pFunc)(void*), void* pArg, bool pAutoClear, int32_t pMode) {
    int32_t iResult = 0;
    pthread_t* iThread = static_cast<pthread_t*>(malloc(sizeof(pthread_t*)));
    pthread_attr_t iThreadCam_attr;

    if (iThread == nullptr) {
        LOG_W("GWBASE::GWTHREAD::GWThread::ThreadStart() - fail malloc");
    }

    pthread_attr_init(&iThreadCam_attr);
    pthread_attr_setdetachstate(&iThreadCam_attr, pMode);
    iResult = pthread_create(iThread, &iThreadCam_attr, pFunc, pArg);

    if (iResult != 0) {
        LOG_W("GWBASE::GWTHREAD::GWThread::ThreadStart() - fail pthread_create");
        return nullptr;
    }

    if (pAutoClear == true) {
        free(iThread);
        iThread = nullptr;
    }

    return iThread;
}

void GWBASE::GWTHREAD::GWThread::ThreadJoin() {
    pthread_join(mThread, NULL);
    return;
}

void GWBASE::GWTHREAD::GWThread::ThreadJoin(pthread_t* pThread) {
    pthread_join(*pThread, NULL);
    return;
}