#include "../../include/base/GWBase.h"
//https://devbin.kr/2021/c%eb%a1%9c-log-%eb%82%a8%ea%b8%b0%eb%8a%94-%eb%b0%a9%eb%b2%95-logger-%eb%a7%8c%eb%93%a4%ea%b8%b0/ Log func

// 아주 가끔 비정상적인 동작을 보일 때가 있음.
// 저장하고있는 파일을 찾아가지 않고 로그 데이터를 기록해 깨진파일이 생성 됨.
// 큰 문제는 아니지만, 로그 누락이 발생하고 그 만큼 Trash 파일이 생성 됨. 우선순위는 낮지만 원인 파악 필요.

GWBASE::GWLOG::GWLog::GWLog() {
    this->mLogLevel = LOG_LEVEL;
    gwFile.MakeDirs(LOG_PATH);
    mInitSet = false;
}

GWBASE::GWLOG::GWLog::GWLog(int32_t pLevel) {
    this->mLogLevel = pLevel;
    GWBASE::gwFile.MakeDirs(LOG_PATH);
    mInitSet = false;
}

bool GWBASE::GWLOG::GWLog::InitLog(int32_t pLevel) {
    mLogName = processName.c_str();
    mLogName += "_";
    mLogName += LOG_NAME;

    mLogMtx = new GWMutex();
    if (mLogMtx->InitMutex("Log") != GWBASE::GWBaseErrorType::GW_NOERROR) {
        return false;
    }
    mSigMtx = new GWMutex();
    if (mLogMtx->InitMutex("FileSig") != GWBASE::GWBaseErrorType::GW_NOERROR) {
        return false;
    }

    GWBASE::GWFILE::LogShiftArg* iArg = new GWBASE::GWFILE::LogShiftArg;

    iArg->mFileClass = &GWBASE::gwFile;
    iArg->mPath = LOG_PATH;
    iArg->mFileName = &mLogName;
    iArg->mCount = LOG_FILE_MAX_COUNT;
    iArg->mSigMtx = mSigMtx;
    iArg->mLogMtx = mLogMtx;

    GWBASE::GWTHREAD::GWThread::ThreadStart(GWBASE::GWFILE::GWFile::ShiftFileName, iArg);

    // sema is being Fixing errors... do not use.
    // mLogSema = new GWSemaphore(iName);

    if ((pLevel == LOG_LEVEL_ALL) || ((pLevel >= LOG_LEVEL_OFF) && (pLevel <= LOG_LEVEL_DEBUGGING))) {
        this->mLogLevel = pLevel;
        mInitSet = true;
        return true;
    }

    return false;
}

std::string GWBASE::GWLOG::GWLog::getTimestamp() {
    std::string strbuff;
    struct timeval tv;
    char buff[300];
    gettimeofday(&tv,NULL);
    tm *t = localtime(&(tv.tv_sec));

    t->tm_year -= 100;
    t->tm_mon += 1;

    strbuff.clear();
    memset(buff, 0, sizeof(buff));
    sprintf(buff + strlen(buff), "%02d", t->tm_year);
    sprintf(buff + strlen(buff), "%02d", t->tm_mon);
    sprintf(buff + strlen(buff), "%02d_", t->tm_mday);
    sprintf(buff + strlen(buff), "%02d:", t->tm_hour);
    sprintf(buff + strlen(buff), "%02d:", t->tm_min);
    sprintf(buff + strlen(buff), "%02d." , t->tm_sec);
    sprintf(buff + strlen(buff), "%06ld" , tv.tv_usec);
    strbuff.append(buff, strlen(buff));

    return strbuff;
}

void GWBASE::GWLOG::GWLog::writeLog(const char* pFileName, const char *pFuncName, int32_t pLine, int32_t pLevel, const char *pStr, ...) {
    std::string iTime = getTimestamp();
    int32_t iFileNamePos = 0;
    int32_t iFileSize = 0;
    FILE* iFp = NULL;
    char *result = NULL;
    char level[6];
    va_list args;
    std::string iThreadId = std::to_string(pthread_self());

    if (mInitSet == false) {
        return;
    }

    // sema is being Fixing errors... do not use.
    //mLogSema->lock();

    mLogMtx->Lock();

    for (iFileNamePos = strlen(pFileName); iFileNamePos > 0; iFileNamePos--) {
        if (*(pFileName + iFileNamePos) == '/') {
            iFileNamePos++;
            break;
        }
    }

    iFileSize = GWBASE::gwFile.GetFileSize(LOG_PATH, mLogName);
    if ((iFileSize < 0) && (iFileSize != (int32_t)GWBaseErrorType::GW_FILE_OPEN_FAIL)) {
        mLogMtx->UnLock();
        return;
    }
    if (iFileSize >= LOG_FILE_MAX_SIZE) {
        mSigMtx->CondSig();
    }

    gwFile.OpenFile(&iFp, LOG_PATH, mLogName, "a");
    if(iFp == NULL) {
        puts("fail to open file pointer");
        exit(1);
        return;
    }

    memset(level, 0, sizeof(level));

    switch(pLevel) {
        case (LOG_LEVEL_ERROR) : {
            strcpy(level, "LOG_E");
            break;
        }
        case (LOG_LEVEL_WARNING) : {
            strcpy(level, "LOG_W");
            break;
        }
        case (LOG_LEVEL_INFORMATION) : {
            strcpy(level, "LOG_I");
            break;
        }
        case (LOG_LEVEL_GENERAL) : {
            strcpy(level, "LOG_G");
            break;
        }
        case (LOG_LEVEL_DEBUGGING) : {
            strcpy(level, "LOG_D");
            break;
        }
    }

    result = (char*)malloc(sizeof(char)*(strlen(level) + iTime.size() + iThreadId.size() + strlen(pFileName + iFileNamePos) + strlen(pFuncName) + strlen(pStr) + 10 + 15 + 1)); //pLine max billion (10) + extra string (10) + last null (1)
    sprintf(result, "%s %s tid:%s [%s:%s:%d] : %s\n", level, iTime.c_str(), iThreadId.c_str(), pFileName + iFileNamePos, pFuncName, pLine, pStr);

    va_start(args, pStr);
    vfprintf(iFp, result, args);
    va_end(args);

    va_start(args, pStr);
    if(this->mLogLevel >= pLevel) {
        if (pLevel == LOG_LEVEL_ERROR) {
            printf("%c[1;31m", 27);     // Set red log output to console
        } else if (pLevel == LOG_LEVEL_WARNING) {
            printf("%c[1;33m", 27);     // Set yellow log output to console
        } else if (pLevel == LOG_LEVEL_DEBUGGING) {
            printf("%c[1;32m", 27);     // Set green log output to console
        }
        vprintf(result, args);
        printf("%c[0m",27);             // Reset to default color
    }
    va_end(args);


    if(result != NULL) {
        free(result);
    }
    if(iFp != NULL) {
        fclose(iFp);
    }

    // sema is being Fixing errors... do not use.
    //mLogSema->unlock();

    mLogMtx->UnLock();

    return;
}