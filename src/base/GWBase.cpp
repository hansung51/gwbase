#include "../../include/base/GWBase.h"

using namespace GWBASE;
std::string GWBASE::processName;
GWLOG::GWLog GWBASE::gwLog;
GWFILE::GWFile GWBASE::gwFile;

GWBase::GWBase() {
    mMainHandler = this;
    mMainHandler->SetLooper(this);
}

void GWBASE::GWBase::HandleMessage(GWMESSAGE::GWMessage* msg) {
    LOG_W("GWBase::handleMessage() some error! this handler is in GWBase.cpp!");
    uint iFunc = msg->GetCallFunc();
    switch (iFunc) {
        default :
            break;
    }
}

GWBASE::GWHANDLER::GWHandler* GWBase::GetHandler() {
    return mMainHandler;
}

void GWBASE::BaseInit(const char* pName, const int32_t pLogLevel) {
    if (pName == nullptr) {
        printf("Invalid param\n");
        exit(1);
    }
    for (int32_t i = strlen(pName); i > 0; i--) {
        if (*(pName + i) == '/') {
            processName = pName + i + 1;
            break;
        }
    }
    gwFile.InitFile();
    gwLog.InitLog(pLogLevel);
}
