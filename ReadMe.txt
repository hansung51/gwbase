!!!Read Me!!!

copyright Copyright (c) 2022 by Park gun woo
author: Park gun woo
E-mail:
    main: hansung51@daum.net
    sub:  rjsdn9112@gmail.com
    sub:  hansung51@mensakorea.org
brief: Framework to use as base code.

---------------------------------------------------------
Description

This framework work on Linux.

This framework is for All users for non-commercial use.

GWBase is supports basic functions for make new project.
If you run GWBase's looper, it hold on and waits for event messages to occur.
You can running looper by occur an event, and one event message is take one task.

If you want know how to use detail, see example source code and patch note in the Information_for_developer folder

---------------------------------------------------------

How to build (Dynamic library)

use CMake

*Tree*
root
├── build (make yourself)
│   └── libGWBase.so (If you successfully make)
├── func_reference
├── include
├── Information_for_developer
├── src
├── CMakeLists.txt
└── ReadMe.txt

* Go to the root folder of this project
* ---- command ----
* mkdir build
* cd build
* cmake ..
* make
* -----------------
* Check to successfully made libGWBase.so in build folder

---------------------------------------------------------

How to include your project

GWBase is provided as a dynamic library.
If you want use, libGWBase.so file be linked, and the header file be include to the your project.
Header file is located in the include folder in the root folder of this project.
For more information on how to use the dynamic library, please see Tutorial 0_setup.txt in Information_for_developer folder or search google.

---------------------------------------------------------

How to use

See example source code and patch note in the Information_for_developer folder
