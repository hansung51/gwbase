/**
 * @file 5_IPC_object.cpp
 * @brief for GWBase tutorial
 * @version used GWBase version 1.4.1.0
 * @date 2022-10-04
 *
 */

#include "../include/base/GWBase.h"

/*
커널에 message queue 를 요청할 ID 값
*/
enum class IpcKey{
    IPC_M_Q_FOO = 10000
};

enum class EventMsgType {
    ARG1 = 0,
    ARG2,
    OBJ,
    OBJ_ARG1,
    OBJ_ARG2,
    CALLBACK
};

class FooClass : public GWBASE::GWTHREAD::GWThread, public GWBASE::GWBase {
public:
    void OnInit(GWBASE::GWIPC::GWIpc<FooClass>* pIpc);
private:
    // virtual func
    void ThreadRun();
    void HandleMessage(GWBASE::GWMESSAGE::GWMessage* msg);

    GWBASE::GWIPC::GWIpc<FooClass>* mIpc;
};

void FooClass::OnInit(GWBASE::GWIPC::GWIpc<FooClass>* pIpc) {
    LOG_D("FooClass::ThreadRun() - Main thread Init!");
    LooperInit();
    mIpc = pIpc;

    /*
    IPC Message queue로 전달할 Argument struct
    */
    GWBASE::IPCMsgBuff iIpcMsg;

    /*
    전달할 parameter
    */
    int32_t iArg1 = 777;

    /*
    MakeMsg 함수를 통해 iIpcMsg 필드에 값을 채운다
    MakeMsg Argument
        - iIpcMsg : 전달할 Argument struct
        - 0 :       Callback IPC Key  (이후 예제에서 다룸)
        - 0 :       Callback function (이후 예제에서 다룸)
        - iArg1 :   전달할 Int형 Argument

    SendMsg 함수를 통해 Kernel에 IPC Message 전달
    SendMsg Argument
        - IpcKey::IPC_M_Q_FOO : 전달할 target이 가진 IPC Key
        - EventMsgType::ARG1 :  전달할 target이 가진 GWBase message queue가 호출할 handler function key
        - iIpcMsg :             Target에 전달할 Argument struct

    MakeMsg에 전달하는 Callback 관련 Argument는 NULL을 넣어도 되지만 특정 Argument를 넣는 경우 함수 호출에 모호성이 발생하므로 0을 넣는 것을 추천함
    */
    mIpc->MakeMsg(iIpcMsg, 0, 0, iArg1)->SendMsg(static_cast<int32_t>(IpcKey::IPC_M_Q_FOO), static_cast<int32_t>(EventMsgType::ARG1), iIpcMsg);

    // 이후 같은 설명은 생략
}

void FooClass::ThreadRun() {
    LOG_D("FooClass::ThreadRun() - Main thread run!");
    LooperRun();
}

void FooClass::HandleMessage(GWBASE::GWMESSAGE::GWMessage* msg) {
    LOG_D("FooClass::HandleMessage - Task start!");
    GWBASE::GWMESSAGE::GWMessageObj iObj = msg->GetObj();
    int32_t iFunc = msg->GetCallFunc();

    switch (iFunc) {
        case static_cast<uint32_t>(EventMsgType::ARG1) : {
            /*
            OnInit() 에서 전달한 IPC message
            커널을 통해 IPCMsgBuff가 전달되고 IPC thread에서 이 곳(target)의 handler에 GWMessage 형태로 전달 된다.
            */
            LOG_D("FooClass::HandleMessage - ARG1");
            LOG_D("FooClass::HandleMessage - msg->arg1 : %d", msg->GetArg1());
            sleep(1);

            GWBASE::IPCMsgBuff iIpcMsg;
            int32_t iArg1 = 888;
            int32_t iArg2 = 999;

            /*
            IPCMsgBuff 에 int형 Argument 2개 전달
            IpcKey::IPC_M_Q_FOO target으로 EventMsgType::ARG2 호출
            */
            mIpc->MakeMsg(iIpcMsg, 0, 0, iArg1, iArg2)->SendMsg(static_cast<int32_t>(IpcKey::IPC_M_Q_FOO), static_cast<int32_t>(EventMsgType::ARG2), iIpcMsg);
            break;
        }

        case static_cast<uint32_t>(EventMsgType::ARG2) : {
            /*
            EventMsgType::ARG1 에서 전달한 IPC message
            */
            LOG_D("FooClass::HandleMessage - ARG2");
            LOG_D("FooClass::HandleMessage - msg->arg1 : %d, msg->arg2 : %d", msg->GetArg1(), msg->GetArg2());
            sleep(1);

            GWBASE::IPCMsgBuff iIpcMsg;
            /*
            malloc 또는 new 키워드로 할당한 경우는 free 필요
            */
            char* iText1 = "hello";
            char* iText2 = static_cast<char*>(malloc(6));
            memset(iText2, 0, 6);
            strcpy(iText2, "world");

            /*
            IPCMsgBuff 에 포인터형 Argument 2개 전달. 최대 4개까지 지원. 1개당 최대 사이즈는 512 byte.                                                 ┌─ ex. struct TO_COPY {char* member};
            전달할 argument는 예시와 달리 문자열이 아닌 것을 보내도 되지만 IPC message를 process간 통신에 이용할 경우, 복사할 pointer자료의 내부 구조에 Pointer형이 포함된 경우는 사용하지 못함. (일반적으로 IPC message는 process간 통신에 이용함) (process간 pointer를 사용하지 못하는 이유는 Kernel의 process memory 구조 확인)
            IpcKey::IPC_M_Q_FOO target으로 EventMsgType::OBJ 호출
            */
            mIpc->MakeMsg(iIpcMsg, 0, 0, iText1, strlen(iText1), iText2, strlen(iText2))->SendMsg(static_cast<int32_t>(IpcKey::IPC_M_Q_FOO), static_cast<int32_t>(EventMsgType::OBJ), iIpcMsg);

            /*
            malloc 또는 new 키워드로 할당한 iText2는 직접 free를 해주어야 메모리 릭이 발생하지 않음
            */
            free(iText2);
            break;
        }

        case static_cast<uint32_t>(EventMsgType::OBJ) : {
            /*
            EventMsgType::ARG2 에서 전달한 IPC message
            */
            LOG_D("FooClass::HandleMessage - OBJ");
            LOG_D("FooClass::HandleMessage - msg->mObject.vPointer1 : %s, msg->mObject.vPointer2 : %s", iObj.vPointer1, iObj.vPointer2);
            sleep(1);

            GWBASE::IPCMsgBuff iIpcMsg;
            char* iText1 = "GWBase";
            int32_t iArg1 = 123;

            /*
            IPCMsgBuff 에 int형 Argument 1개와 포인터형 Argument 1개 전달. 포인터형은 최대 4개까지 지원. 포인터형 1개당 최대 사이즈는 512 byte.
            IpcKey::IPC_M_Q_FOO target으로 EventMsgType::OBJ_ARG1 호출
            */
            mIpc->MakeMsg(iIpcMsg, 0, 0, iArg1, iText1, strlen(iText1))->SendMsg(static_cast<int32_t>(IpcKey::IPC_M_Q_FOO), static_cast<int32_t>(EventMsgType::OBJ_ARG1), iIpcMsg);
            break;
        }

        case static_cast<uint32_t>(EventMsgType::OBJ_ARG1) : {
            /*
            EventMsgType::OBJ 에서 전달한 IPC message
            */
            LOG_D("FooClass::HandleMessage - OBJ_ARG1");
            LOG_D("FooClass::HandleMessage - msg->arg1 : %d, msg->mObject.vPointer1 : %s", msg->GetArg1(), static_cast<char*>(iObj.vPointer1));
            sleep(1);

            GWBASE::IPCMsgBuff iIpcMsg;
            char* iText1 = "GWBase";
            int32_t iArg1 = 456;
            int32_t iArg2 = 789;

            /*
            IPCMsgBuff 에 int형 Argument 2개와 포인터형 Argument 1개 전달. 포인터형은 최대 4개까지 지원. 포인터형 1개당 최대 사이즈는 512 byte.
            IpcKey::IPC_M_Q_FOO target으로 EventMsgType::OBJ_ARG2 호출
            */
            mIpc->MakeMsg(iIpcMsg, 0, 0, iArg1, iArg2, iText1, strlen(iText1))->SendMsg(static_cast<int32_t>(IpcKey::IPC_M_Q_FOO), static_cast<int32_t>(EventMsgType::OBJ_ARG2), iIpcMsg);
            break;
        }

        case static_cast<uint32_t>(EventMsgType::OBJ_ARG2) : {
            /*
            EventMsgType::OBJ_ARG1 에서 전달한 IPC message
            */
            LOG_D("FooClass::HandleMessage - OBJ_ARG2");
            LOG_D("FooClass::HandleMessage - msg->arg1 : %d, msg->mObject.vPointer1 : %s, msg->mObject.vPointer2 : %s", msg->GetArg1(), static_cast<char*>(iObj.vPointer1), static_cast<char*>(iObj.vPointer2));
            sleep(1);

            GWBASE::IPCMsgBuff iIpcMsg;

            /*
            IPCMsgBuff 에 Callback을 요청할 Target 의 IPC Key와 target의 GWBase message queue가 호출할 handler function key 전달
            호출할 Task에서 일련의 동작 수행 후 값을 리턴 받을 필요가 있거나, Task 동작에 이어서 특정한 다른 Task를 호출할 필요가 있을 경우 사용 함
            IpcKey::IPC_M_Q_FOO target으로 EventMsgType::CALLBACK 호출
            */
            mIpc->MakeMsg(iIpcMsg, static_cast<int32_t>(IpcKey::IPC_M_Q_FOO), static_cast<int32_t>(EventMsgType::ARG1))->SendMsg(static_cast<int32_t>(IpcKey::IPC_M_Q_FOO), static_cast<int32_t>(EventMsgType::CALLBACK), iIpcMsg);
            break;
        }

        case static_cast<uint32_t>(EventMsgType::CALLBACK) : {
            /*
            EventMsgType::CALLBACK 에서 전달한 IPC message
            basic tutorial - 3_msg_object.cpp에서 언급하지 않고 넘어간 msg->mObject.callbackManagerKey 와 msg->mObject.callbackFunc 가 사용된다
            EventMsgType::CALLBACK 에서 iIpcMsg 에 set 한 argument (IPC key, Func key)가 각각 callbackManagerKey와 callbackFunc에 할당된다
            */
            LOG_D("FooClass::HandleMessage - CALLBACK");
            LOG_D("FooClass::HandleMessage - msg->mObject.callbackManagerKey : %d, msg->mObject.callbackFunc : %d", iObj.callbackManagerKey, iObj.callbackFunc);
            sleep(1);

            GWBASE::IPCMsgBuff iIpcMsg;
            int32_t iArg1 = 1;

            /*
            호출할 Task에서 일련의 동작 수행 후 이전 Task에서 요청한 Target과 function 호출.
            EventMsgType::OBJ_ARG2에서 요청한 내용과 같이 IpcKey::IPC_M_Q_FOO target으로 EventMsgType::ARG1 호출
            */
            mIpc->MakeMsg(iIpcMsg, 0, 0, iArg1)->SendMsg(iObj.callbackManagerKey, iObj.callbackFunc, iIpcMsg);
            break;
        }

        default: {
            break;
        }
    }

    LOG_D("FooClass::HandleMessage - Task end!");
}

int main(int argv, char** argc) {
    GWBASE::BaseInit(argc[0], LOG_LEVEL);
    LOG_I("GWBASE version:             %s", GW_BASE_VERSION);
    FooClass* mFoo = new FooClass;
    GWBASE::GWIPC::GWIpc<FooClass>* mIpc = new GWBASE::GWIPC::GWIpc(mFoo);

    mFoo->OnInit(mIpc);
    mIpc->OnInit(static_cast<int32_t>(IpcKey::IPC_M_Q_FOO));
    mFoo->ThreadStart();
    mIpc->ThreadStart();
    mFoo->ThreadJoin();
    mIpc->ThreadJoin();

    return 0;
}