/**
 * @file 3_msg_object.cpp
 * @brief for GWBase tutorial
 * @version used GWBase version 1.4.1.0
 * @date 2022-09-20
 *
 */

#include "../include/base/GWBase.h"

enum class EventMsgType {
    ARG1 = 0,
    ARG2,
    OBJ,
    OBJ_ARG1,
    OBJ_ARG2,
    COMPLEX_OBJ,
};

/*
    COMPLEX_OBJ 예제에서 사용 될 구조체
*/
struct COMPLEXOBJ {
    std::string* mStr;
};

class FooClass : public GWBASE::GWTHREAD::GWThread, public GWBASE::GWBase {
public:
    void OnInit();
private:
    // virtual func
    void ThreadRun();
    void HandleMessage(GWBASE::GWMESSAGE::GWMessage* msg);
};

void FooClass::OnInit() {
    LOG_D("FooClass::ThreadRun() - Main thread Init!");
    LooperInit();

    /*
    ARG1 Evnet 발생
    Event message 에 int type의 argument 1개 전달.
    */
    int32_t iArg1 = 123;
    GWBASE::GWMESSAGE::GWMessage* iMsg = this->GetHandler()->ObtainMsg();
    if (iMsg != nullptr) {
        iMsg->SetMsg(static_cast<int32_t>(EventMsgType::ARG1), iArg1)->SendToTarget();
    }

}

void FooClass::ThreadRun() {
    LOG_D("FooClass::ThreadRun() - Main thread run!");
    LooperRun();
}

void FooClass::HandleMessage(GWBASE::GWMESSAGE::GWMessage* msg) {
    LOG_D("FooClass::HandleMessage - Task start!");
    GWBASE::GWMESSAGE::GWMessageObj iObj = msg->GetObj();
    int32_t iFunc = msg->GetCallFunc();

    switch (iFunc) {
        case static_cast<uint32_t>(EventMsgType::ARG1) : {
            LOG_D("FooClass::HandleMessage - ARG1");

            /*
            Event message 에서 같이 전달 받은 Argument 를 읽어 출력함.
            발생시킨 EvnetMsgType::ARG1 message 를 참고하여, int type의 argument를 전달하면 msg->GetArg1() 함수로 꺼내올 수 있다.
            */
            LOG_D("FooClass::HandleMessage - msg->arg1 : %d", msg->GetArg1());
            sleep(1);

            GWBASE::GWMESSAGE::GWMessage* iMsg = this->GetHandler()->ObtainMsg();
            if (iMsg != nullptr) {
                /*
                ARG2 Evnet 발생
                Event message 에 int type의 argument 2개 전달.
                */
                int32_t iArg1 = 123;
                int32_t iArg2 = 456;
                iMsg->SetMsg(static_cast<int32_t>(EventMsgType::ARG2), iArg1, iArg2)->SendToTarget();
            }
            break;
        }

        case static_cast<uint32_t>(EventMsgType::ARG2) : {
            LOG_D("FooClass::HandleMessage - ARG2");
            /*
            Event message 에서 같이 전달 받은 Argument 를 읽어 출력함.
            EvnetMsgType::ARG1 에서 발생시킨 event message 를 참고하여, int type의 argument를 전달하면 msg->GetArg1() 함수로 꺼내올 수 있다.
            두 번째 int type 인자를 꺼낼 때는 msg->GetArg2() 함수를 이용한다.
            */
            LOG_D("FooClass::HandleMessage - msg->arg1 : %d, msg->arg2 : %d", msg->GetArg1(), msg->GetArg2());
            sleep(1);

            GWBASE::GWMESSAGE::GWMessage* iMsg = this->GetHandler()->ObtainMsg();
            if (iMsg != nullptr) {
                /*
                OBJ Evnet 발생
                Event message 에 GWMessageObj struct를 전달.

                struct GWMessageObj {
                    void* vPointer1;                =====> nullptr로 초기화 되어있음.
                    void* vPointer2;                =====> nullptr로 초기화 되어있음.
                    void* vPointer3;                =====> nullptr로 초기화 되어있음.
                    void* vPointer4;                =====> nullptr로 초기화 되어있음.
                    int32_t callbackManagerKey;     =====> 이후 예제에서 다룸.
                    int32_t callbackFunc;           =====> 이후 예제에서 다룸.
                }
                */
                GWBASE::GWMESSAGE::GWMessageObj iSendObj;
                iSendObj.vPointer1 = new std::string("vPointer1");
                int32_t* vPointer_int = static_cast<int32_t*>(malloc(sizeof(int32_t)));
                *vPointer_int = 777;
                iSendObj.vPointer2 = vPointer_int;
                iMsg->SetMsg(static_cast<int32_t>(EventMsgType::OBJ), iSendObj)->SendToTarget();
            }
            break;
        }

        case static_cast<uint32_t>(EventMsgType::OBJ) : {
            LOG_D("FooClass::HandleMessage - OBJ");
            /*
            Event message 에서 같이 전달 받은 object argument 를 읽어 출력함.
            EvnetMsgType::ARG2 에서 발생시킨 event message 를 참고하여, object argument를 전달하면 msg->GetObj() 함수로 꺼내올 수 있다.
            Task 종료 시점에 nullptr이 아닌 Obj.vPointer는 자동으로 free 됨.
            만약 Task 도중에 free 한다면 double free crash가 발생함.

            struct GWMessageObj {
                void* vPointer1;                =====> nullptr이 아닌 경우 Task 종료 시점에 자동 free.
                void* vPointer2;                =====> nullptr이 아닌 경우 Task 종료 시점에 자동 free.
                void* vPointer3;                =====> nullptr이 아닌 경우 Task 종료 시점에 자동 free.
                void* vPointer4;                =====> nullptr이 아닌 경우 Task 종료 시점에 자동 free.
                int32_t callbackManagerKey;
                int32_t callbackFunc;
            }
            */
            std::string* iObjStr = static_cast<std::string*>(iObj.vPointer1);
            int32_t* iObjInt = static_cast<int32_t*>(iObj.vPointer2);
            LOG_D("FooClass::HandleMessage - msg->obj.vPointer1 : %s, msg->obj.vPointer2 : %d", iObjStr->c_str(), *iObjInt);
            /*
            활성화 하면 double free crash 발생
            */
            #if 0
            free(iObjInt);
            #endif
            sleep(1);

            GWBASE::GWMESSAGE::GWMessage* iMsg = this->GetHandler()->ObtainMsg();
            if (iMsg != nullptr) {
                /*
                OBJ_ARG1 Evnet 발생
                Event message 에 GWMessageObj struct와 int type의 argument 1개 전달.
                */
                int32_t iArg1 = 12;
                GWBASE::GWMESSAGE::GWMessageObj iSendObj;
                iSendObj.vPointer1 = new std::string("vPointer1");

                iMsg->SetMsg(static_cast<int32_t>(EventMsgType::OBJ_ARG1), iSendObj, iArg1)->SendToTarget();
            }
            break;
        }

        case static_cast<uint32_t>(EventMsgType::OBJ_ARG1) : {
            LOG_D("FooClass::HandleMessage - OBJ_ARG1");
            /*
            Event message 에서 같이 전달 받은 object argument 를 읽어 출력함.
            EvnetMsgType::OBJ 에서 발생시킨 event message 를 참고하여,
            object argument를 전달하면 msg->GetObj() 함수로 꺼내올 수 있고 int type의 argument를 전달하면 msg->GetArg1() 함수로 꺼내올 수 있다.
            nullptr이 아닌 iObj.vPointer는 Task 종료 시 자동으로 free.
            */
            int32_t iArg1 = msg->GetArg1();
            std::string* iObjStr = static_cast<std::string*>(iObj.vPointer1);
            LOG_D("FooClass::HandleMessage - msg->obj.vPointer1 : %s, msg->arg1 : %d", iObjStr->c_str(), iArg1);
            sleep(1);

            GWBASE::GWMESSAGE::GWMessage* iMsg = this->GetHandler()->ObtainMsg();
            if (iMsg != nullptr) {
                /*
                OBJ_ARG2 Evnet 발생
                Event message 에 GWMessageObj struct와 int type의 argument 2개 전달.
                */
                int32_t iArg1 = 12;
                int32_t iArg2 = 34;
                GWBASE::GWMESSAGE::GWMessageObj iSendObj;
                iSendObj.vPointer1 = new std::string("vPointer1");

                iMsg->SetMsg(static_cast<int32_t>(EventMsgType::OBJ_ARG2), iSendObj, iArg1, iArg2)->SendToTarget();
            }
            break;
        }

        case static_cast<uint32_t>(EventMsgType::OBJ_ARG2) : {
            LOG_D("FooClass::HandleMessage - OBJ_ARG2");
            /*
            Event message 에서 같이 전달 받은 object argument 를 읽어 출력함.
            EvnetMsgType::OBJ_ARG1 에서 발생시킨 event message 를 참고하여,
            object argument를 전달하면 msg->GetObj() 함수로 꺼내올 수 있고 int type의 argument를 전달하면 msg->GetArg1() 함수로 꺼내올 수 있다.
            두 번째 int type 인자를 꺼낼 때는 msg->GetArg2() 함수를 이용한다.
            nullptr이 아닌 iObj.vPointer는 Task 종료 시 자동으로 free.
            */
            int32_t iArg1 = msg->GetArg1();
            int32_t iArg2 = msg->GetArg2();
            std::string* iObjStr = static_cast<std::string*>(iObj.vPointer1);
            LOG_D("FooClass::HandleMessage - msg->obj.vPointer1 : %s, msg->arg1 : %d, msg->arg2", iObjStr->c_str(), iArg1, iArg2);
            sleep(1);

            GWBASE::GWMESSAGE::GWMessage* iMsg = this->GetHandler()->ObtainMsg();
            if (iMsg != nullptr) {
                GWBASE::GWMESSAGE::GWMessageObj iSendObj;
                COMPLEXOBJ* iCObj = static_cast<COMPLEXOBJ*>(malloc(sizeof(COMPLEXOBJ)));
                iCObj->mStr = new std::string("iCObj->mStr");

                iSendObj.vPointer1 = new std::string("vPointer1");
                iSendObj.vPointer2 = iCObj;

                iMsg->SetMsg(static_cast<int32_t>(EventMsgType::COMPLEX_OBJ), iSendObj)->SendToTarget();
            }
            break;
        }

        case static_cast<uint32_t>(EventMsgType::COMPLEX_OBJ) : {
            LOG_D("FooClass::HandleMessage - COMPLEX_OBJ");
            /*
            Event message 에서 같이 전달 받은 object argument 를 읽어 구조체를 가져 옴.
            EvnetMsgType::OBJ_ARG2 에서 발생시킨 event message 를 참고하여, object argument를 전달하면 msg->GetObj() 함수로 꺼내올 수 있다.
            이번 경우에서 object.pointer2 는 COMPLEXOBJ 구조체의 형태를 하고있고, COMPLEXOBJ 는 다음과 같은 구조를 가진다.
            COMPLEXOBJ {
                std::string* mStr;
            }

            다시 설명해 이번 Task 에서 msg->obj 는 다음과 같다.
            struct GWMessageObj {
                void* vPointer1; (std::string*)         =====> nullptr이 아닌 경우 Task 종료 시점에 자동 free.
                void* vPointer2; (COMPLEXOBJ*)         =====> nullptr이 아닌 경우 Task 종료 시점에 자동 free.
                      vPointer2->COMPLEXOBJ*->std::string*    ====> nullptr이 아니어도 종료 시점에 자동 free 되지 않음. 직접 free 하여 메모리 관리 필요.
                                                                    (이후 버전에서는 이와 비슷한 경우도 조건 만족시 자동 free 될 수 있도록 지원할 예정)
                void* vPointer3;                       =====> nullptr이 아닌 경우 Task 종료 시점에 자동 free.
                void* vPointer4;                       =====> nullptr이 아닌 경우 Task 종료 시점에 자동 free.
                int32_t callbackManagerKey;
                int32_t callbackFunc;
            }
            */
            std::string* iObjStr = static_cast<std::string*>(iObj.vPointer1);
            COMPLEXOBJ* iCObj = static_cast<COMPLEXOBJ*>(iObj.vPointer2);
            LOG_D("FooClass::HandleMessage - msg->obj.vPointer1 : %s, msg->obj.vPointer2->mStr : %s", iObjStr->c_str(), iCObj->mStr->c_str());
            sleep(1);

            /*
            iCObj->mStr 은 자동으로 free되지 않으니 직접 자원관리 필요.
            iCObj 은 자동으로 free 됨.

            이후 업데이트에서는 특정 조건 만족시 이러한 경우도 자동 free 될 수 있도록 구현될 예정.
            */
            free(iCObj->mStr);

            /*
            end program
            비활성화 하면 프로그램은 event message를 기다리며 무한 대기함.
            */
            #if 1
            LOG_D("!!!End program!!!");
            exit(0);
            #endif
            break;
        }

        default: {
            break;
        }
    }

    LOG_D("FooClass::HandleMessage - Task end!");
}

int main(int argv, char** argc) {
    GWBASE::BaseInit(argc[0], LOG_LEVEL);
    LOG_I("GWBASE version:             %s", GW_BASE_VERSION);
    FooClass* mFoo = new FooClass;
    mFoo->OnInit();
    mFoo->ThreadStart();
    mFoo->ThreadJoin();

    return 0;
}