/**
 * @file 1_hello_world.cpp
 * @brief for GWBase tutorial
 * @version used GWBase version 1.4.1.0
 * @date 2022-09-13
 *
 */

/*
Copy the entire include folder and include it in your project.
*/
#include "../include/base/GWBase.h"

int main(int argv, char** argc) {
    /*
    If you use GWBase, first must be set initializing option

    first option is process name
    second option is log level for console output

    LOG_LEVEL is defined in '{folder you set}/base/UserDefine/Log.h'
    And you can find '#define LOG_LEVEL               LOG_LEVEL_ALL'
    If you want to be change log level, define like as LOG_LEVEL_INFORMATION (level type is defined in the line above)
    */
    GWBASE::BaseInit(argc[0], LOG_LEVEL);
    LOG_I("GWBASE version:             %s", GW_BASE_VERSION);

    /*
    macro of LOG_D (and LOG_E, W, I, G) is process the output you define
    this macro will be save all output in the log file, and output to console according to LOG_LEVEL
    if you define LOG_LEVEL highter then LOG_LEVEL_DEBUGGING, you can see 'LOG_D! hello, world!' in console
    and you can see save log in the /home/{user name}/data/log/{process name}_Log.txt
    also you can define save log path and name, by redefine line 7, 9 in Log.h
    don't worry if it's not a pre-generated path! smartly creates a folder according to the specified path to save the log

    the log contents are organized as follow
    LOG_LEVEL YYMMDD_HH:MM:SS.mmmmmm tid:thread_ID [file_name:func_name:line] : output
        - eg. LOG_D 220913_14:57:57.732952 tid:139681467352896 [1_hello_world.cpp:main:53] : LOG_D! hello, world!

    Did you check the log file storage and console output?
    now, define LOG_LEVEL lower then LOG_LEVEL_GENERAL, and rebuild
    you can see save log in the /home/{user name}/data/log/{process name}_Log.txt, but you won't see the console output 'LOG_D! hello, world!'
    popularly, console output is time consuming. so in the release version, console output should be reduced as much as possible
    you can regulate console output by redefine LOG_LEVEL

    the maximum size of the log file follow line 31 of Log.h
    if you want to change the maximum size of the log file, redefine the that line
    */
    LOG_E("LOG_E! hello, world! %d", 100);
    LOG_W("LOG_W! hello, world! %s", "abcd");
    LOG_I("LOG_I! hello, world! %f", 1.234);
    LOG_G("LOG_G! hello, world! %d %s", 200, "qwer");
    LOG_D("LOG_D! hello, world!");

    return 0;
}