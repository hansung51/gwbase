/**
 * @file 2_make_looper.cpp
 * @brief for GWBase tutorial
 * @version used GWBase version 1.4.1.0
 * @date 2022-09-14
 *
 */

#include "../include/base/GWBase.h"

enum class EventMsgType {
    PING = 0,
    PONG
};

/*
    유저가 정의한 Main thread 객체입니다
    GWBASE::GWBase 을 상속받음으로 looper, message queue, handler를 사용합니다.
    위 세가지는 유기적으로 얽혀있어 각 기능별로 사용하기 어렵습니다.(불가능하지 않음!)
    Looper를 사용하기 위해서는 thread 기능을 추가로 상속하여야 합니다.
*/
class FooClass : public GWBASE::GWTHREAD::GWThread, public GWBASE::GWBase {
public:
    // user define func
    void OnInit();
private:
    // virtual func
    void ThreadRun();
    void HandleMessage(GWBASE::GWMESSAGE::GWMessage* msg);

    // user define func
    void PingPong(EventMsgType pSendType);
};

void FooClass::OnInit() {
    LOG_D("FooClass::ThreadRun() - Main thread Init!");
    /*
    Looper를 사용하기 위해서는 반드시 초기화가 필요합니다.
    LooperInit 함수를 호출하여 초기화를 진행할 수 있습니다.
    */
    LooperInit();

    /*
    이벤트 메시지 발생 (반드시 LooperInit 이후에 이벤트를 발생시켜야 함)
    */
    PingPong(EventMsgType::PING);
}

void FooClass::ThreadRun() {
    LOG_D("FooClass::ThreadRun() - Main thread run!");
    /*
    Looper를 무한루프로 대기시킴.
    이벤트 발생이 감지되면 cpu 자원을 획득하여 Task 실행. Task 종료되면 cpu에 자원 반납.
    */
    LooperRun();
}

/*
Main thread handler
Event가 발생하면 제일 먼저 이 함수를 호출하여 Event에 알맞는 Task를 실행한다.
*/
void FooClass::HandleMessage(GWBASE::GWMESSAGE::GWMessage* msg) {
    LOG_D("FooClass::HandleMessage - Task start!");
    GWBASE::GWMESSAGE::GWMessageObj iObj = msg->GetObj();
    int32_t iFunc = msg->GetCallFunc();

    switch (iFunc) {
        //Task case PING
        case static_cast<uint32_t>(EventMsgType::PING) : {
            LOG_D("FooClass::HandleMessage - PING");
            sleep(1);
            PingPong(EventMsgType::PONG);
            break;
        }

        //Task case PONG
        case static_cast<uint32_t>(EventMsgType::PONG) : {
            LOG_D("FooClass::HandleMessage - PONG");
            sleep(1);
            PingPong(EventMsgType::PING);
            break;
        }

        default: {
            break;
        }
    }

    LOG_D("FooClass::HandleMessage - Task end!");
}

void FooClass::PingPong(EventMsgType pSendType) {
    /*
    FooClass가 가진 Handler를 타깃, 미리 준비되어있는 이벤트 메시지 컨테이너에서 비어있는 공간을 가져옴
    */
    GWBASE::GWMESSAGE::GWMessage* iMsg = this->GetHandler()->ObtainMsg();
    if (iMsg != nullptr) {
        LOG_D("FooClass::PingPong() - Make event!");
        /*
        메시지 포멧에 데이터를 설정하고 이벤트를 발생시킴
        */
        iMsg->SetMsg(static_cast<uint32_t>(pSendType))->SendToTarget();
    }
}

int main(int argv, char** argc) {
    GWBASE::BaseInit(argc[0], LOG_LEVEL);
    LOG_I("GWBASE version:             %s", GW_BASE_VERSION);

    /*
    Main thread 객체 생성
    */
    FooClass* mFoo = new FooClass;

    /*
    Initializing
    객체 초기 설정 및 Looper 초기 설정 진행
    */
    mFoo->OnInit();

    /*
    Threading
    */
    mFoo->ThreadStart();

    /*
    Thread 가 종료 될 때까지 대기
    */
    mFoo->ThreadJoin();

    return 0;
}