/**
 * @file 4_IPC_message.cpp
 * @brief for GWBase tutorial
 * @version used GWBase version 1.4.1.0
 * @date 2022-09-20
 *
 */

#include "../include/base/GWBase.h"

/*
커널에 message queue 를 요청할 ID 값
*/
enum class IpcKey{
    IPC_M_Q_FOO = 10000
};

enum class EventMsgType {
    IPC_PING = 0,
    IPC_PONG
};

class FooClass : public GWBASE::GWTHREAD::GWThread, public GWBASE::GWBase {
public:
    void OnInit(GWBASE::GWIPC::GWIpc<FooClass>* pIpc);
private:
    // virtual func
    void ThreadRun();
    void HandleMessage(GWBASE::GWMESSAGE::GWMessage* msg);

    // user define func
    void PingPong(EventMsgType pSendType);

    GWBASE::GWIPC::GWIpc<FooClass>* mIpc;
};

void FooClass::OnInit(GWBASE::GWIPC::GWIpc<FooClass>* pIpc) {
    LOG_D("FooClass::ThreadRun() - Main thread Init!");
    LooperInit();
    mIpc = pIpc;

    GWBASE::GWMESSAGE::GWMessage* iMsg = this->GetHandler()->ObtainMsg();
    if (iMsg != nullptr) {
        iMsg->SetMsg(static_cast<int32_t>(EventMsgType::IPC_PING))->SendToTarget();
    }
}

void FooClass::ThreadRun() {
    LOG_D("FooClass::ThreadRun() - Main thread run!");
    LooperRun();
}

void FooClass::HandleMessage(GWBASE::GWMESSAGE::GWMessage* msg) {
    LOG_D("FooClass::HandleMessage - Task start!");
    GWBASE::GWMESSAGE::GWMessageObj iObj = msg->GetObj();
    int32_t iFunc = msg->GetCallFunc();

    switch (iFunc) {
        case static_cast<uint32_t>(EventMsgType::IPC_PING) : {
            LOG_D("FooClass::HandleMessage - IPC_PING");
            PingPong(EventMsgType::IPC_PONG);
            sleep(1);
            break;
        }

        case static_cast<uint32_t>(EventMsgType::IPC_PONG) : {
            LOG_D("FooClass::HandleMessage - IPC_PONG");
            PingPong(EventMsgType::IPC_PING);
            sleep(1);
            break;
        }

        default: {
            break;
        }
    }

    LOG_D("FooClass::HandleMessage - Task end!");
}

void FooClass::PingPong(EventMsgType pSendType) {
    /*
    IPC Message queue로 전달할 Argument
    */
    GWBASE::IPCMsgBuff iIpcMsg;

    /*
    MakeMsg() 로 전달 Argument에 값 set.
    SendMsg() 를 통해 msg를 전달시킬 IPC Queue를 지정하고, 호출할 Handler func을 지정함.

    IPC Message queue 는 커널을 경유해 전달되기 때문에 느린 속도를 가짐.
    프로세스간 통신을 위해 사용되며, 이 마저도 큰 데이터를 전달할 때는 IPC Message queue 모델은 적합하지 않음.

    예제에서는 단일 프로세스 환경에서 IPC Msg 통신을 보이고 있지만, 같은 프로세스 내 통신이라면 Handler event msg가 월등한 속도를 보인다.
    다중 프로세스간 통신에서 사용하길 권장.
    */
    mIpc->MakeMsg(iIpcMsg)->SendMsg(static_cast<int32_t>(IpcKey::IPC_M_Q_FOO), static_cast<int32_t>(pSendType), iIpcMsg);
}

int main(int argv, char** argc) {
    GWBASE::BaseInit(argc[0], LOG_LEVEL);
    LOG_I("GWBASE version:             %s", GW_BASE_VERSION);
    FooClass* mFoo = new FooClass;
    /*
    IPC Message queue 사용.
    GWIpc 를 사용하는 class type 설정 필요.
    */
    GWBASE::GWIPC::GWIpc<FooClass>* mIpc = new GWBASE::GWIPC::GWIpc(mFoo);

    /*
    사용자가 정의하는 OnInit() 함수의 파라미터에 GWIpc 입력 설정
    */
    mFoo->OnInit(mIpc);

    /*
    커널이 할당해줄 IPC Message queue의 식별 Key 값 정의하여 전달.
    */
    mIpc->OnInit(static_cast<int32_t>(IpcKey::IPC_M_Q_FOO));

    /*
    Thread start
        Thread 1 - Main thread
        Thread 2 - IPC thread
    */
    mFoo->ThreadStart();
    mIpc->ThreadStart();
    mFoo->ThreadJoin();
    mIpc->ThreadJoin();

    return 0;
}