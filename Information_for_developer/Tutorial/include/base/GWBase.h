/**
 * @file GWBase.h
 * @author Park gun woo
 *          E-mail, main: hansung51@daum.net
 *                  sub:  rjsdn9112@gmail.com
 *                  sub:  hansung51@mensakorea.org
 * @brief Framework to use as base code.
 * @version 1.4.1
 * @date 2022-07-26
 *
 * @copyright Copyright (c) 2022 by Park gun woo
 * Free for non-commercial use
 *
 */

#ifndef GWBASE_H
#define GWBASE_H

#include <iostream>
#include <stdint.h>
#include <string>
#include <string.h>
#include <mutex>
#include <stdio.h>
#include <fstream>
#include <cstdarg>
#include <unistd.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sstream>

#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include "./UserDefine/Log.h"

#include "./CoreDefine/CoreDefine.h"
#include "./CoreDefine/GWBaseVer.h"

/**
 *  @brief LOG_E - Used to log by fatal errors.
 *
 *  @param format Format of texts to be output
 *  @param va_arg variable arguments
 */
#define LOG_E(format, ...) \
        GWBASE::gwLog.writeLog(__FILE__, __FUNCTION__, __LINE__, LOG_LEVEL_ERROR, format, ##__VA_ARGS__)

/**
 *  @brief LOG_W - Used to log by warning, when a non-fatal error occurs.
 *
 *  @param format Format of texts to be output
 *  @param va_arg variable arguments
 */
#define LOG_W(format, ...) \
        GWBASE::gwLog.writeLog(__FILE__, __FUNCTION__, __LINE__, LOG_LEVEL_WARNING, format, ##__VA_ARGS__)

/**
 *  @brief LOG_I - Used to log by information, A typical log that shows to the user.
 *
 *  @param format Format of texts to be output
 *  @param va_arg variable arguments
 */
#define LOG_I(format, ...) \
        GWBASE::gwLog.writeLog(__FILE__, __FUNCTION__, __LINE__, LOG_LEVEL_INFORMATION, format, ##__VA_ARGS__)

/**
 *  @brief LOG_G - Used to log by normal. for developer.
 *
 *  @param format Format of texts to be output
 *  @param va_arg variable arguments
 */
#define LOG_G(format, ...) \
        GWBASE::gwLog.writeLog(__FILE__, __FUNCTION__, __LINE__, LOG_LEVEL_GENERAL, format, ##__VA_ARGS__)

/**
 *  @brief LOG_D - Used to log by debug. for developer.
 *
 *  @param format Format of texts to be output
 *  @param va_arg variable arguments
 */
#define LOG_D(format, ...) \
        GWBASE::gwLog.writeLog(__FILE__, __FUNCTION__, __LINE__, LOG_LEVEL_DEBUGGING, format, ##__VA_ARGS__)

namespace GWBASE
{
    namespace GWFILE {
        class GWFile {
        public:
            GWFile();

            bool InitFile();

            int32_t GetFileSize(const char* pPath, const char* pName);
            int32_t GetFileSize(const char* pPath, const std::string  pName);
            int32_t GetFileSize(const char* pPath, const std::string* pName);
            int32_t GetFileSize(const std::string  pPath, const char* pName);
            int32_t GetFileSize(const std::string* pPath, const char* pName);
            int32_t GetFileSize(const std::string  pPath, std::string  pName);
            int32_t GetFileSize(const std::string  pPath, std::string* pName);
            int32_t GetFileSize(const std::string* pPath, std::string  pName);
            int32_t GetFileSize(const std::string* pPath, std::string* pName);

            static void* ShiftFileName(void* pArg);
            GWBASE::GWBaseErrorType ShiftFileName(const char* pPath, const char* pName, const uint32_t pShiftCount);
            GWBASE::GWBaseErrorType ShiftFileName(const char* pPath, const std::string  pName, const uint32_t pShiftCount);
            GWBASE::GWBaseErrorType ShiftFileName(const char* pPath, const std::string* pName, const uint32_t pShiftCount);
            GWBASE::GWBaseErrorType ShiftFileName(const std::string  pPath, const char* pName, const uint32_t pShiftCount);
            GWBASE::GWBaseErrorType ShiftFileName(const std::string* pPath, const char* pName, const uint32_t pShiftCount);
            GWBASE::GWBaseErrorType ShiftFileName(const std::string  pPath, const std::string  pName, const uint32_t pShiftCount);
            GWBASE::GWBaseErrorType ShiftFileName(const std::string  pPath, const std::string* pName, const uint32_t pShiftCount);
            GWBASE::GWBaseErrorType ShiftFileName(const std::string* pPath, const std::string  pName, const uint32_t pShiftCount);
            GWBASE::GWBaseErrorType ShiftFileName(const std::string* pPath, const std::string* pName, const uint32_t pShiftCount);

            GWBASE::GWBaseErrorType MakeDirs(const char* pPath);
            GWBASE::GWBaseErrorType MakeDirs(const std::string  pPath);
            GWBASE::GWBaseErrorType MakeDirs(const std::string* pPath);

            GWBASE::GWBaseErrorType OpenFile(FILE** pFile, const char* pPath, const char* pName, const char* pMode);
            GWBASE::GWBaseErrorType OpenFile(FILE** pFile, const char* pPath, const std::string  pName, const char* pMode);
            GWBASE::GWBaseErrorType OpenFile(FILE** pFile, const char* pPath, const std::string* pName, const char* pMode);
            GWBASE::GWBaseErrorType OpenFile(FILE** pFile, const std::string  pPath, const char* pName, const char* pMode);
            GWBASE::GWBaseErrorType OpenFile(FILE** pFile, const std::string* pPath, const char* pName, const char* pMode);
            GWBASE::GWBaseErrorType OpenFile(FILE** pFile, const std::string  pPath, const std::string  pName, const char* pMode);
            GWBASE::GWBaseErrorType OpenFile(FILE** pFile, const std::string  pPath, const std::string* pName, const char* pMode);
            GWBASE::GWBaseErrorType OpenFile(FILE** pFile, const std::string* pPath, const std::string  pName, const char* pMode);
            GWBASE::GWBaseErrorType OpenFile(FILE** pFile, const std::string* pPath, const std::string* pName, const char* pMode);

        private:
            void MakeShiftName(std::string* pFileName, const char* pPath, const char* pName, const int32_t pNum);

            bool mInitSet;
            GWMutex* mFileMtx;
        };

        struct LogShiftArg {
            GWFile* mFileClass;
            char* mPath;
            std::string* mFileName;
            int32_t mCount;
            GWMutex* mLogMtx;
        };
    } // namespace GWFile
    extern GWFILE::GWFile gwFile;

    namespace GWLOG {
        class GWLog {
        public:
            GWLog();
            GWLog(int32_t pLevel);
            bool InitLog(int32_t pLevel);
            void writeLog(const char* pFileName, const char* pFuncName, int32_t pLine, int32_t pLevel, const char* pStr, ...);
        private:
            std::string getTimestamp();

            int32_t mLogLevel;
            bool mIsOutput;
            std::string mLogName;
            // sema is Fixing errors... not use
            GWSemaphore* mLogSema;
            GWMutex* mLogMtx;
            bool mInitSet;
        };
    } // namespace GWLOG
    extern GWLOG::GWLog gwLog;

    namespace GWMESSAGE {
        class GWMessage;
    }

    namespace GWLOOPER {
        class GWLooper {
        public:
            void LooperRun();
            void LooperInit();
            void LooperLock();
            void LooperUnlock();
            void LooperWakeUp();
            uint8_t GetSize();
            void IncreaseSize();
            void IncreaseIndex();
            void SetIndexToBuff(uint32_t pIndex);
            GWMESSAGE::GWMessage* GetMsgInContainer();
            uint8_t GetMsgBufSize();
            void IncreaseMsgBufSize();
            void DecreaseMsgBufSize();
            uint8_t GetMsgIndex();
            void IncreaseMsgIndex();

        private:
            std::mutex mLoopSetMutex;
            pthread_mutex_t mLooperSleepMtx;
            uint8_t mLoopBufSize;
            uint8_t mLoopGetIndex;
            uint8_t mLoopSetIndex;
            uint32_t LooperBuff[MESSAGE_BUFF_SIZE];
            GWMESSAGE::GWMessage* messageContainer[MESSAGE_BUFF_SIZE];
            uint8_t mMsgBufSize;
            uint8_t mMsgSetIndex;
        };
    } // namespace GWLOOPER

    namespace GWHANDLER {
        class GWHandler {
        public:
            GWHandler();
            void SetLooper(GWLOOPER::GWLooper* pLooper);
            virtual void HandleMessage(GWMESSAGE::GWMessage* msg);
            GWMESSAGE::GWMessage* ObtainMsg();
        private:
            std::mutex mMsgSetMutex;
            GWLOOPER::GWLooper* mLooper;
        };
    } // namespace GWHANDLER

    namespace GWMESSAGE {
        class GWMessage {
        public:
                GWMessage();
                GWMessage(GWLOOPER::GWLooper* pLooper);
                GWMessage(GWLOOPER::GWLooper* pLooper, GWMessage* msg);
                void InitMsg(GWHANDLER::GWHandler* pHandler, uint32_t pIndex);
                GWMessage* SetMsg(uint32_t CallFunc);
                GWMessage* SetMsg(uint32_t CallFunc, uint64_t arg1, uint64_t arg2 = 0);
                GWMessage* SetMsg(uint32_t CallFunc, GWMessageObj object);
                GWMessage* SetMsg(uint32_t CallFunc, GWMessageObj object, uint64_t arg1, uint64_t arg2 = 0);
                bool SendToTarget();
                GWHANDLER::GWHandler* GetHandler();
                uint32_t GetIndex();
                bool GetIsUse();
                uint32_t GetCallFunc();
                uint64_t GetArg1();
                uint64_t GetArg2();
                GWMessageObj GetObj();
                void MsgClear();
                void FreeObj();
        private:
                GWHANDLER::GWHandler* mHandler;
                uint32_t mIndex;
                bool mIsUse;
                uint32_t mCallFunc;
                uint64_t mArg1;
                uint64_t mArg2;
                GWMessageObj mObject;
                GWLOOPER::GWLooper* mLooper;
        };
    } // namespace GWMESSAGE

    namespace GWTHREAD {
        class GWThread {
        public:
                GWThread();
                ~GWThread();
                virtual void ThreadRun();
                // pthread_t* 리턴으로 변경해야 함
                bool ThreadStart();
                /**
                * @brief Creates an independent thread
                *
                * @param pFunc callback function
                * @param pArg callback function argument
                * @param pMode pthread_attr_t mode. default val: PTHREAD_CREATE_DETACHED. see parameter __detachstate of pthread_attr_setdetachstate in pthreads.h
                * @return If thread creat sucessed return true, or failed return false
                */
                static bool ThreadStart(void* (*pFunc)(void*), void* pArg, int32_t pMode = PTHREAD_CREATE_DETACHED);
                void ThreadJoin();
                pthread_attr_t mAttr;
        private:
                static void* _run(void*);

                pthread_t mThread;
        };
    } // namespace GWTHREAD

    class GWBase : public GWLOOPER::GWLooper, public GWMESSAGE::GWMessage, public GWHANDLER::GWHandler {
    public:
        GWBase();
        GWBASE::GWHANDLER::GWHandler* GetHandler();
        virtual void HandleMessage(GWMESSAGE::GWMessage* msg);
    private:
        GWBASE::GWHANDLER::GWHandler* mMainHandler;
    };

    // GWSemaphore is being Fixing critical errors... plz do not use.
    class GWSemaphore {
    public:
        /**
        *    @param pSemaName   semaphore name.
        *                       If you not set pFlag, semaphore name set 'process name' + 'pSemaName'.
        *                       If you set pFlag, semaphore name set 'pSemaName'.
        *    @param pFlag       If you want to share semaphore with between separate process, set true. default set false.
        *    @param pVal        semaphore param val. default 1.
        *    @param pTimeOutSec semaphore timeout val. default 1.
        */
        GWSemaphore(const char* pSemaName, bool pFlag = false , uint32_t pVal = 1, time_t pTimeOutSec = 1);

        bool Lock();
        bool UnLock();
    private:
        bool CheckProcessDupl(const std::string& pSemaName);

        sem_t* mSem;
        uint32_t mVal;
        struct timespec mTm;
        time_t mTimeOut;
        std::string mSemaName;
    };

    // https://kldp.org/node/107186
    typedef struct shm_struct {
        pthread_mutex_t  mtx;
        pthread_cond_t   cond;
    } shm_struct_t;

    class GWMutex {
    public:
        GWMutex();
        GWBASE::GWBaseErrorType InitMutex(const char* pMtxName);
        bool Lock();
        bool UnLock();
    private:
        pthread_mutexattr_t mLogMutexAtt;
        pthread_condattr_t mLogCondAtt;
        shm_struct_t* g_shm;
        bool mInitSet;
    };

    namespace GWIPC{
        template <typename T_Class>
        class GWIpc : public GWTHREAD::GWThread {
        public:
            GWIpc(T_Class* pManager);
            ~GWIpc();
            void OnInit(uint32_t pMyIpcKey);

            // rcv Ipc Message part

            void ThreadRun();

            // snd Ipc Message part
            GWIpc* MakeMsg(IPCMsgBuff& pSendData);
            GWIpc* MakeMsg(IPCMsgBuff& pSendData, void* pPointer1, uint32_t pPLen1, void* pPointer2 = nullptr, uint32_t pPLen2 = 0, void* pPointer3 = nullptr, uint32_t pPLen3 = 0, void* pPointer4 = nullptr, uint32_t pPLen4 = 0);
            GWIpc* MakeMsg(IPCMsgBuff& pSendData, int32_t pCallbackManagerkey, int32_t pCallbackFunc);
            GWIpc* MakeMsg(IPCMsgBuff& pSendData, int32_t pCallbackManagerkey, int32_t pCallbackFunc, void* pPointer1, uint32_t pPLen1, void* pPointer2 = nullptr, uint32_t pPLen2 = 0, void* pPointer3 = nullptr, uint32_t pPLen3 = 0, void* pPointer4 = nullptr, uint32_t pPLen4 = 0);
            GWIpc* MakeMsg(IPCMsgBuff& pSendData, int32_t pCallbackManagerkey, int32_t pCallbackFunc, int64_t pArg1);
            GWIpc* MakeMsg(IPCMsgBuff& pSendData, int32_t pCallbackManagerkey, int32_t pCallbackFunc, int64_t pArg1, void* pPointer1, uint32_t pPLen1, void* pPointer2 = nullptr, uint32_t pPLen2 = 0, void* pPointer3 = nullptr, uint32_t pPLen3 = 0, void* pPointer4 = nullptr, uint32_t pPLen4 = 0);
            GWIpc* MakeMsg(IPCMsgBuff& pSendData, int32_t pCallbackManagerkey, int32_t pCallbackFunc, int64_t pArg1, int64_t pArg2);
            GWIpc* MakeMsg(IPCMsgBuff& pSendData, int32_t pCallbackManagerkey, int32_t pCallbackFunc, int64_t pArg1, int64_t pArg2, void* pPointer1, uint32_t pPLen1, void* pPointer2 = nullptr, uint32_t pPLen2 = 0, void* pPointer3 = nullptr, uint32_t pPLen3 = 0, void* pPointer4 = nullptr, uint32_t pPLen4 = 0);
            uint32_t SendMsg(uint32_t pToMgrKey, uint32_t pCallFunc, IPCMsgBuff pSendData);

        private:
            T_Class* mManager;
            key_t mKeyId;
            IPCMsgBuff mIpcMsg;
        };
    } // GWIPC

    void BaseInit(const char* pName, const int32_t pLogLevel);
} // namespace GWBASE


template <typename T_Class>
GWBASE::GWIPC::GWIpc<T_Class>::GWIpc(T_Class* pManager) {
    mManager = pManager;
}

template <typename T_Class>
GWBASE::GWIPC::GWIpc<T_Class>::~GWIpc() {
    msgctl(mKeyId, IPC_RMID, NULL);
}

template <typename T_Class>
void GWBASE::GWIPC::GWIpc<T_Class>::OnInit(uint32_t pMyIpcKey) {
    msgctl(pMyIpcKey, IPC_RMID, 0);
    mKeyId = msgget(pMyIpcKey, IPC_CREAT|0666);
    if (mKeyId < 0) {
        LOG_E("detected some error!");
        exit(1);
    }
    LOG_D("IPC mKeyId %d", mKeyId);
}

template <typename T_Class>
void GWBASE::GWIPC::GWIpc<T_Class>::ThreadRun() {
    while (true) {
        if (msgrcv(mKeyId, (void *)&mIpcMsg, sizeof(struct _IPCMsgBuff), 0, 0) == -1) {
            LOG_W("detected some error!");
        } else {
            uint32_t iCallFunc = mIpcMsg.callFunc;
            GWBASE::GWMESSAGE::GWMessageObj iObj;
            iObj.callbackFunc = mIpcMsg.callbackFunc;
            iObj.callbackManagerKey = mIpcMsg.callbackManagerKey;
            iObj.vPointer1 = (void*)malloc(sizeof(mIpcMsg.data1));
            iObj.vPointer2 = (void*)malloc(sizeof(mIpcMsg.data2));
            iObj.vPointer3 = (void*)malloc(sizeof(mIpcMsg.data3));
            iObj.vPointer4 = (void*)malloc(sizeof(mIpcMsg.data4));
            memset(iObj.vPointer1, 0, sizeof(mIpcMsg.data1));
            memset(iObj.vPointer2, 0, sizeof(mIpcMsg.data2));
            memset(iObj.vPointer3, 0, sizeof(mIpcMsg.data3));
            memset(iObj.vPointer4, 0, sizeof(mIpcMsg.data4));
            memcpy(iObj.vPointer1, mIpcMsg.data1, sizeof(mIpcMsg.data1));
            memcpy(iObj.vPointer2, mIpcMsg.data2, sizeof(mIpcMsg.data2));
            memcpy(iObj.vPointer3, mIpcMsg.data3, sizeof(mIpcMsg.data3));
            memcpy(iObj.vPointer4, mIpcMsg.data4, sizeof(mIpcMsg.data4));
            GWBASE::GWMESSAGE::GWMessage* msg = mManager->GetHandler()->ObtainMsg();
            if (msg != nullptr) {
                msg->SetMsg(iCallFunc, iObj, mIpcMsg.arg1, mIpcMsg.arg2)->SendToTarget();
            } else {
                LOG_W("GWBASE::GWIPC::GWIpc<T_Class>::ThreadRun() - msg is nullprt");
            }
        }
    }
}

template <typename T_Class>
GWBASE::GWIPC::GWIpc<T_Class>* GWBASE::GWIPC::GWIpc<T_Class>::MakeMsg(IPCMsgBuff& pSendData) {
    return MakeMsg(pSendData, -1, -1, 0, 0, nullptr, 0, nullptr, 0, nullptr, 0, nullptr, 0);
}

template <typename T_Class>
GWBASE::GWIPC::GWIpc<T_Class>* GWBASE::GWIPC::GWIpc<T_Class>::MakeMsg(IPCMsgBuff& pSendData, void* pPointer1, uint32_t pPLen1, void* pPointer2, uint32_t pPLen2, void* pPointer3, uint32_t pPLen3, void* pPointer4, uint32_t pPLen4) {
    return MakeMsg(pSendData, -1, -1, 0, 0, pPointer1, pPLen1, pPointer2, pPLen2, pPointer3, pPLen3, pPointer4, pPLen4);
}

template <typename T_Class>
GWBASE::GWIPC::GWIpc<T_Class>* GWBASE::GWIPC::GWIpc<T_Class>::MakeMsg(IPCMsgBuff& pSendData, int32_t pCallbackManagerkey, int32_t pCallbackFunc) {
    return MakeMsg(pSendData, pCallbackManagerkey, pCallbackFunc, 0, 0, nullptr, 0, nullptr, 0, nullptr, 0, nullptr, 0);
}

template <typename T_Class>
GWBASE::GWIPC::GWIpc<T_Class>* GWBASE::GWIPC::GWIpc<T_Class>::MakeMsg(IPCMsgBuff& pSendData, int32_t pCallbackManagerkey, int32_t pCallbackFunc, void* pPointer1, uint32_t pPLen1, void* pPointer2, uint32_t pPLen2, void* pPointer3, uint32_t pPLen3, void* pPointer4, uint32_t pPLen4) {
    return MakeMsg(pSendData, pCallbackManagerkey, pCallbackFunc, 0, 0, pPointer1, pPLen1, pPointer2, pPLen2, pPointer3, pPLen3, pPointer4, pPLen4);
}

template <typename T_Class>
GWBASE::GWIPC::GWIpc<T_Class>* GWBASE::GWIPC::GWIpc<T_Class>::MakeMsg(IPCMsgBuff& pSendData, int32_t pCallbackManagerkey, int32_t pCallbackFunc, int64_t pArg1) {
    return MakeMsg(pSendData, pCallbackManagerkey, pCallbackFunc, pArg1, 0, nullptr, 0, nullptr, 0, nullptr, 0, nullptr, 0);

}

template <typename T_Class>
GWBASE::GWIPC::GWIpc<T_Class>* GWBASE::GWIPC::GWIpc<T_Class>::MakeMsg(IPCMsgBuff& pSendData, int32_t pCallbackManagerkey, int32_t pCallbackFunc, int64_t pArg1, void* pPointer1, uint32_t pPLen1, void* pPointer2, uint32_t pPLen2, void* pPointer3, uint32_t pPLen3, void* pPointer4, uint32_t pPLen4) {
    return MakeMsg(pSendData, pCallbackManagerkey, pCallbackFunc, pArg1, 0, pPointer1, pPLen1, pPointer2, pPLen2, pPointer3, pPLen3, pPointer4, pPLen4);
}

template <typename T_Class>
GWBASE::GWIPC::GWIpc<T_Class>* GWBASE::GWIPC::GWIpc<T_Class>::MakeMsg(IPCMsgBuff& pSendData, int32_t pCallbackManagerkey, int32_t pCallbackFunc, int64_t pArg1, int64_t pArg2) {
    return MakeMsg(pSendData, pCallbackManagerkey, pCallbackFunc, pArg1, pArg2, nullptr, 0, nullptr, 0, nullptr, 0, nullptr, 0);
}

template <typename T_Class>
GWBASE::GWIPC::GWIpc<T_Class>* GWBASE::GWIPC::GWIpc<T_Class>::MakeMsg(IPCMsgBuff& pSendData, int32_t pCallbackManagerkey, int32_t pCallbackFunc, int64_t pArg1, int64_t pArg2, void* pPointer1, uint32_t pPLen1, void* pPointer2, uint32_t pPLen2, void* pPointer3, uint32_t pPLen3, void* pPointer4, uint32_t pPLen4) {
    pSendData.callbackManagerKey = pCallbackManagerkey;
    pSendData.callbackFunc = pCallbackFunc;
    pSendData.arg1 = pArg1;
    pSendData.arg2 = pArg2;
    memset(pSendData.data1, 0, IPC_BUFF_SIZE);
    memset(pSendData.data2, 0, IPC_BUFF_SIZE);
    memset(pSendData.data3, 0, IPC_BUFF_SIZE);
    memset(pSendData.data4, 0, IPC_BUFF_SIZE);
    if (pPointer1 != nullptr) {
        if (pPLen1 > IPC_BUFF_SIZE) {
            LOG_W("the pPointer1 size is over then %dbyte, failed data copy", IPC_BUFF_SIZE);
        } else {
            memcpy(pSendData.data1, pPointer1, pPLen1);
        }
    }
    if (pPointer2 != nullptr) {
        if (pPLen2 > IPC_BUFF_SIZE) {
            LOG_W("the pPointer2 size is over then %dbyte, failed data copy", IPC_BUFF_SIZE);
        } else {
            memcpy(pSendData.data2, pPointer2, pPLen2);
        }
    }
    if (pPointer3 != nullptr) {
        if (pPLen3 > IPC_BUFF_SIZE) {
            LOG_W("the pPointer3 size is over then %dbyte, failed data copy", IPC_BUFF_SIZE);
        } else {
            memcpy(pSendData.data3, pPointer3, pPLen3);
        }
    }
    if (pPointer4 != nullptr) {
        if (pPLen4 > IPC_BUFF_SIZE) {
            LOG_W("the pPointer4 size is over then %dbyte, failed data copy", IPC_BUFF_SIZE);
        } else {
            memcpy(pSendData.data4, pPointer4, pPLen4);
        }
    }

    return this;
}

template <typename T_Class>
uint32_t GWBASE::GWIPC::GWIpc<T_Class>::SendMsg(uint32_t pToMgrKey, uint32_t pCallFunc, IPCMsgBuff pSendData) {
    uint32_t iResult = static_cast<uint32_t>(GWBASE::GWBaseErrorType::GW_NOERROR);

    pSendData.msgType = pToMgrKey;
    pSendData.callFunc = pCallFunc;
    key_t iKeyId = msgget(pToMgrKey, IPC_CREAT|0666);
    if (iKeyId == -1) {
        LOG_E("detected msgget func some error! iKeyId == -1");
        exit(1);
    }

    //if (msgsnd(iKeyId, &pSendData, sizeof(GWBASE::_IPCMsgBuff), IPC_NOWAIT) == -1) {
    if (msgsnd(iKeyId, &pSendData, sizeof(GWBASE::_IPCMsgBuff), 0) == -1) {
        LOG_E("detected msgsnd func some error! msgsnd()");
        exit(1);
    }

    return iResult;
}

#endif // GWBASE_H