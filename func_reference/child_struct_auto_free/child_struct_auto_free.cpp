#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

typedef struct ST {
protected:
    ST *mParent;
    char mChildFlag;
    std::string mName;
    struct Child {
        void *mP1;
        void *mP2;
        void *mP3;
        void *mP4;
    };
    Child mChild;

public:
    ST(std::string pName);
    enum struct PointerIndex { mP1 = 0, mP2 = 1, mP3 = 2, mP4 = 3, END };
    void Clear();
    void Print();
    const char* GetName() const;
    void* GetChild(PointerIndex pIndex) const;
    bool InsertPointer(void *pP, PointerIndex pIndex);
    bool InsertPointer(ST *pP, PointerIndex pIndex);
} ST;

typedef struct ST_TMP : public ST {
    ST_TMP(std::string pName);
    int32_t a;
    char *b;
    int32_t c;
    void temp() {
        return;
    }

private:
    double d;
    void temp2() {
        return;
    }
} ST_TMP;

typedef struct ST_TMP2 {
    ST_TMP2(std::string pName);
    int32_t a;
    char *b;
    int32_t c;
    void temp() {
        return;
    }

private:
    double d;
    void temp2() {
        return;
    }
} ST_TMP2;

ST::ST(std::string pName) {
    printf("make and init %s, this pointer %p\n", pName.c_str(), this);
    mParent = nullptr;
    mName = pName;
    mChildFlag = 0x00;
    mChild.mP1 = nullptr;
    mChild.mP2 = nullptr;
    mChild.mP3 = nullptr;
    mChild.mP4 = nullptr;
}

ST_TMP::ST_TMP(std::string pName) : ST(pName) {
}

ST_TMP2::ST_TMP2(std::string pName) {
}

void ST::Clear() {
    printf("DBG: Clear()++ name: %s\n", mName.c_str());
    void **offset = &(mChild.mP1);
    void **parentOffset = nullptr;
    bool iCheck = false;

    if (mParent != nullptr) {
        printf("DBG: mParent is not null! mParent : %p\n", mParent);
        parentOffset = &(mParent->mChild.mP1);
        for (int32_t i = 0; i < static_cast<int32_t>(ST::PointerIndex::END); i++) {
            printf("DBG: parentOffset : %p, this : %p\n", *parentOffset, this);
            if (*parentOffset == this) {
                printf("DBG: find me on parent! parentOffset : %p, this : %p\n", *parentOffset, this);
                printf("DBG: unset before pmParent->mChildFlag : 0x%02x\n", mParent->mChildFlag);
                mParent->mChildFlag &= ~(0x01 << i);
                printf("DBG: unset after pmParent->mChildFlag : 0x%02x\n", mParent->mChildFlag);
                break;
            }
        parentOffset++;
        }
    }

    for (int i = 0; i < static_cast<int32_t>(ST::PointerIndex::END); i++) {
        printf("DBG: offset : %p, mChildFlag : 0x%02x\n", *offset, mChildFlag);
        if (*offset != nullptr) {
            printf("DBG: mChildFlag : 0x%02x, check : 0x%02x, result : %d\n", mChildFlag, (0x01 << i), (mChildFlag & (0x01 << i)));
            if ((bool)(mChildFlag & (0x01 << i)) == true) {
                printf("DBG: this offset is ST type. offset : %p\n", *offset);
                (*((ST **)offset))->Clear();
            } else {
                printf("DBG: this offset is not ST type. offset : %p\n", *offset);
                free(*offset);
            }
        }
        offset++;
    }
    free(this);
    printf("DBG: Clear()--\n");
}

bool ST::InsertPointer(void *pP, PointerIndex pIndex) {
    int32_t iIndex = static_cast<int32_t>(pIndex);
    printf("DBG: InsertPointer - void*, iIndex = %d, pP = %p\n", iIndex, pP);

    switch (iIndex) {
        case 0: { // PointerIndex::mP1
            this->mChild.mP1 = pP;
            printf("DBG: InsertPointer - mChild.mP1 = %p\n", mChild.mP1);
            break;
        }
        case 1: { // PointerIndex::mP2
            this->mChild.mP2 = pP;
            printf("DBG: InsertPointer - mChild.mP2 = %p\n", mChild.mP2);
            break;
        }
        case 2: { // PointerIndex::mP3
            this->mChild.mP3 = pP;
            printf("DBG: InsertPointer - mChild.mP3 = %p\n", mChild.mP3);
            break;
        }
        case 3: { // PointerIndex::mP4
            this->mChild.mP4 = pP;
            printf("DBG: InsertPointer - mChild.mP4 = %p\n", mChild.mP4);
            break;
        }
    }
    return true;
}
bool ST::InsertPointer(ST *pP, PointerIndex pIndex) {
    int32_t iIndex = static_cast<int32_t>(pIndex);
    printf("DBG: InsertPointer - ST*, iIndex = %d, pP = %p\n", iIndex, pP);
    printf("DBG: %s is in the %s\n", pP->mName.c_str(), this->mName.c_str());

    if (pP == nullptr) {
        return false;
    }

    switch (iIndex) {
        case 0: { // PointerIndex::mP1
            this->mChild.mP1 = pP;
            printf("DBG: InsertPointer - mChild.mP1 = %p\n", mChild.mP1);
            break;
        }
        case 1: { // PointerIndex::mP2
            this->mChild.mP2 = pP;
            printf("DBG: InsertPointer - mChild.mP2 = %p\n", mChild.mP2);
            break;
        }
        case 2: { // PointerIndex::mP3
            this->mChild.mP3 = pP;
            printf("DBG: InsertPointer - mChild.mP3 = %p\n", mChild.mP3);
            break;
        }
        case 3: { // PointerIndex::mP4
            this->mChild.mP4 = pP;
            printf("DBG: InsertPointer - mChild.mP4 = %p\n", mChild.mP4);
            break;
        }
    }

    mChildFlag |= (0x01 << iIndex);
    printf("DBG: mChildFlag : 0x%02x\n", mChildFlag);
    pP->mParent = this;

    return true;
}

void ST::Print() {
    printf("%s member print\n", this->mName.c_str());
    void **offset = &(this->mChild.mP1);
    for (int i = 0; i < static_cast<int32_t>(ST::PointerIndex::END); i++) {
        printf("name : %s, mChild.mP%d: %p\n", this->mName.c_str(), i, *offset);
        offset++;
    }
    printf("\n");
}

const char* ST::GetName() const {
    return this->mName.c_str();
}

void* ST::GetChild(PointerIndex pIndex) const {
    void* ret = nullptr;

    switch(static_cast<int32_t>(pIndex)) {
        case static_cast<int32_t>(ST::PointerIndex::mP1) : {
            ret = mChild.mP1;
            break;
        }
        case static_cast<int32_t>(ST::PointerIndex::mP2) : {
            ret = mChild.mP1;
            break;
        }
        case static_cast<int32_t>(ST::PointerIndex::mP3) : {
            ret = mChild.mP1;
            break;
        }
        case static_cast<int32_t>(ST::PointerIndex::mP4) : {
            ret = mChild.mP1;
            break;
        }
        default : {
            ret = nullptr;
            break;
        }
    }

    return ret;
}

int main() {
    ST *foo = new ST("foo");
    ST *goo = new ST("goo");
    ST *qoo = new ST("qoo");
    ST *woo = new ST("woo");
    ST *eoo = new ST("eoo");
    ST *roo = new ST("roo");
    ST_TMP *too = new ST_TMP("too");
    ST_TMP2 *aoo = new ST_TMP2("aoo");
    int32_t *i = (int *)malloc(sizeof(int32_t));

    foo->InsertPointer(goo, ST::PointerIndex::mP1);
    foo->InsertPointer(i,   ST::PointerIndex::mP2);
    foo->InsertPointer(qoo, ST::PointerIndex::mP3);
    foo->InsertPointer(woo, ST::PointerIndex::mP4);

    goo->InsertPointer(eoo, ST::PointerIndex::mP2);
    goo->InsertPointer(roo, ST::PointerIndex::mP4);

    roo->InsertPointer(too, ST::PointerIndex::mP4);
    roo->InsertPointer(aoo, ST::PointerIndex::mP1);

    foo->Print();
    goo->Print();
    roo->Print();

    printf("start free\n");
    foo->Clear();
    printf("end free\n");

    return 0;
}