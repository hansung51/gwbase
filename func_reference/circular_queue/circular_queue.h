/*
    크기가 정적으로 고정 된 buffer를 circular queue로 사용하여, 크기가 동적인 Data를 저장하기 위해 작성된 코드.

    버퍼의 크기는 정적으로 고정되어 있지만, 쓰여질 데이터의 크기는 동적으로 할당하여 매 동작마다 크기를 알 수 없음.
    쓰여질 데이터는 SHM_NAL_DATA 라고 명명하였으며, SHM_NAL_DATA Header(mDataNum ~ mDataSize)의 크기는 fix 되었으나 Body(mData)의 크기는 모름.
    이러한 상황에서 data를 1byte 단위로 copy. Read / Write case별 동작을 정의.

    Buffer의 size는 Data size에 비해 충분히 주어져 있다고 가정함. (관련 예외처리 과정은 본 코드에서 구현하지 않았음)

    Write
        case 1. SHM_NAL_DATA를 쓰기에 Buffer의 남은 공간이 충분 함.
        case 2. SHM_NAL_DATA를 쓰기엔 Buffer의 남은 공간이 충분하지 않아 Body가 나뉘어짐.
        case 3. SHM_NAL_DATA를 쓰기엔 Buffer의 남은 공간이 충분하지 않아 Header가 나뉘어짐.
        Read function이 실행되지 않아 Write position이 Read position을 침범할 경우,
        이후 Read function이 실행되었을 때 깨진 데이터를 읽지 않도록 Read position을 옮기는 작업을 진행.

    Read
        case 1. Buffer의 현재 Read position 기준, SHM_NAL_DATA Header만큼 memcpy하기에 여유가 충분함.
        case 2. Buffer의 현재 Read position 기준, SHM_NAL_DATA Header가 잘려 보관되어 있음.
        case 3. Buffer의 현재 Read position 기준, SHM_NAL_DATA Body가 잘려 보관되어 있음.
        Read function이 정상 처리되어 return 될 경우,
        다음 실행 때 같은 공간을 읽지 않기 위해 Read position 갱신.
*/

#include "../../include/base/GWBase.h"

// 원형 큐에 기록할 데이터 형식.
// 구조체에 멤버를 추가할 땐 mDataSize 보다 위 쪽으로 멤버를 추가하여야 함. (그보다 아래에 추가할 경우, 아래 코드에서 offset 계산에 문제가 생김)
struct SHM_NAL_DATA {
    uint64_t mDataNum;
    int64_t mPts;
    int64_t mDts;
    int64_t mDuration;
    int64_t mDataSize;
    uint8_t* mData;
};

typedef enum _SHM_RET_TYPE {
    NO_ERROR = 0,
    E_OVER,
    E_NOT_YET,
}SHM_RET_TYPE;


/*
    Shm structure:
        Shm is work as a circular queue
        offset 0 ~ 63(bit) are the information area. this area is fixed.
            - offset 0 ~ 31 : Read Position (uint32_t)
            - offset 32 ~ 63 : Write Position (uint32_t)
        offset 64 ~ eof are the data area. this area is work as a circular queue.

    @param pBuffSize must enter pShm size minus offset bit.
*/
inline bool WriteToShm(uint8_t* pShm, const int32_t pBuffSize, SHM_NAL_DATA* pData, GWBASE::GWMutex* pMtx) {
    // pShm는 shared memory로, 여러 프로세스가 동시에 사용할 수 있는 메모리 공간.
    // Mutex로 다른 process 및 thread의 임계영역(pShm) 동시 접근을 막음
    pMtx->Lock();

    // pShm의 offset 0 ~ 31 주소. uint32_t 형으로 변환하여 iReadPos을 기록하는 공간으로 사용.
    uint32_t* iReadPos  = reinterpret_cast<uint32_t*>(pShm + (sizeof(uint32_t) * 0));
    // pShm의 offset 32 ~ 63 주소. uint32_t 형으로 변환하여 iWritePos을 기록하는 공간으로 사용.
    uint32_t* iWritePos = reinterpret_cast<uint32_t*>(pShm + (sizeof(uint32_t) * 1));
    // pShm의 offset 64 주소. 원형 큐 버퍼로 사용되는 공간.
    uint8_t* iShm       =                             pShm + (sizeof(uint32_t) * 2);
    // SHM_NAL_DATA 헤더 사이즈. mData 를 제외한 크기가 계산 됨.
    uint32_t iInfoSize  = sizeof(SHM_NAL_DATA) - sizeof(pData->mData);
    uint64_t iCopy      = 0;
    int32_t  iCut;

    _reCal:
    iCopy = *iReadPos;

    // *iWritePos 값이 앞서있는 경우, *iReadPos에 pBuffSize만큼 가상공간을 더함.
    // *iWritePos 값이 앞서있다면, 실제로 버퍼에 저장할 여유공간이 있더라도 계산되지 않아 항상 오버플로우로 게산 됨.
    if (iCopy < (*iWritePos + 1)) {
        iCopy += pBuffSize;
    }

    // 가상공간을 고려한 *iReadPos에 앞으로 쓰여질 데이터 크기를 비교, 남은 공간이 충분한지 확인.
    if ((*iWritePos + 1 + iInfoSize + pData->mDataSize) > iCopy) { // 공간이 부족한 경우

        // iShm의 시작주소에서 *iReadPos 만큼 이동한다음 SHM_NAL_DATA* 타입으로 형변환, mDataSize 멤버의 위치로 이동하여 주소 값을 가져옴
        // 가져온 주소 값에 iShm의 시작 주소를 감산하여 헌재 *iReadPos 값의 위치에서 mDataSize 멤버의 offset을 구함
        // 만약 *iReadPos이 pBuffSize 값에 거의 근접하는 경우, 가져온 offset이 pBuffSize를 넘어갈 수 있으므로 pBuffSize로 나눈 나머지 값을 가져옴 (버퍼를 원형큐로 사용하기 때문)
        iCopy = ((reinterpret_cast<uint8_t*>(&(reinterpret_cast<SHM_NAL_DATA*>(iShm + *iReadPos))->mDataSize)) - iShm) % pBuffSize;

        // 계산된 iCopy는 SHM_NAL_DATA 구조체의 mDataSize 멤버를 가르킴.
        // mDataSize는 uint64_t 형이기 때문에, 버퍼의 크기와 비교하여 만약 현재 offset이 8byte의 공간도 남지 않았다면 기록이 가능한 byte만큼 잘려져 보관되어 있을 것.
        // pBuffSize에서 mDataSize size만큼 감산하여 mDataSize가 잘리지 않은 상태로 온전히 보관되어 있을지 확인.
        if (iCopy < (pBuffSize - sizeof(uint64_t))) {

            // 모든 바이트가 온전히 보관되어있는 경우
            // iCopy는 Read position의 정보를 담았지만, 여기서부턴 Data body의 크기 정보를 담음.
            iCopy = *(reinterpret_cast<int64_t*>(iShm + iCopy));
        } else {

            // 일부 바이트가 잘려서 보관되어 있는 경우
            // iCopy는 read position의 정보를 담았지만, 여기서부턴 Data body의 크기 정보를 담음.
            iCut = reinterpret_cast<uint8_t*>(&(reinterpret_cast<SHM_NAL_DATA*>(iShm + *iReadPos))->mDataSize) - (iShm + pBuffSize - sizeof(int64_t));
            memcpy((reinterpret_cast<uint8_t*>(&iCopy)), (reinterpret_cast<uint8_t*>(&(reinterpret_cast<SHM_NAL_DATA*>(iShm + *iReadPos))->mDataSize)), sizeof(int64_t) - iCut);
            memcpy((reinterpret_cast<uint8_t*>(&iCopy)) + sizeof(int64_t) - iCut, iShm, iCut);
        }

        // *iReadPos 갱신
        // 현재 *iReadPos 값에 SHM_NAL_DATA Header size와 mDataSize 합산한 후 버퍼 사이즈만큼 나누어 나머지를 취함.
        *iReadPos = (*iReadPos + sizeof(SHM_NAL_DATA) - sizeof(pData->mData) + iCopy) % pBuffSize;

        // *iReadPos를 갱신하였음에도 복사할 데이터가 다음 *iReadPos를 침범할 수도 있으므로 한번 더 계산. 충분한 공간이 나올 때까지 반복함.
        goto _reCal;
    } else { // 공간이 충분한 경우

        // 크기가 정적으로 고정된 버퍼 크기를 넘어서 데이터를 복사하는 경우인지 확인
        if (pBuffSize > (*iWritePos + iInfoSize + pData->mDataSize)) {

            // SHM_NAL_DATA header와 body 모두 데이터 잘림 없이 한번에 복사 됨. case 1.
            // 들어오는 데이터의 크기에 비해 버퍼의 크기가 충분히 큰 경우, 가장 빈번히 일어남.
            // if문 최적화를 위해 1번 case에 위치.
            LOG_D("WriteToShm() - case 1");
            memcpy(iShm + *iWritePos, pData, iInfoSize);
            memcpy(iShm + *iWritePos + iInfoSize, pData->mData, pData->mDataSize);
            iInfoSize += pData->mDataSize;
            *iWritePos += iInfoSize;
        } else if (pBuffSize > (*iWritePos + iInfoSize)) {

            // SHM_NAL_DATA header는 잘림없이 복사되지만 body는 복사 도중 버퍼의 끝에 다달아 offset 0로 되돌아가 복사 됨. case 2.
            // 들어오는 데이터의 크기에 비해 버퍼의 크기가 충분히 큰 경우, 두 번째로 빈번히 일어남.
            // if문 최적화를 위해 2번 case에 위치.
            LOG_D("WriteToShm() - case 2");
            iCopy = (*iWritePos + iInfoSize + pData->mDataSize) - pBuffSize;
            memcpy(iShm + *iWritePos, pData, iInfoSize);
            memcpy(iShm + *iWritePos + iInfoSize, pData->mData, pData->mDataSize - iCopy);
            memcpy(iShm, pData->mData + pData->mDataSize - iCopy, iCopy);
            *iWritePos = iCopy;
        } else {

            // SHM_NAL_DATA header 복사 도중 버퍼의 끝에 다달아 offset 0로 되돌아가 복사 됨. case 3.
            // header에 비해 body가 클 수록 일어나지 않음. (코드가 입혀진 실제 프로그램에서는 pBuffSize의 크기는 1MB, SHM_NAL_DATA의 header 크기는 40byte, body 크기는 약 250KB...)
            // if문 최적화를 위해 3번 case에 위치.
            LOG_D("WriteToShm() - case 3");
            iCopy = (*iWritePos + iInfoSize) - pBuffSize;
            memcpy(iShm + *iWritePos, pData, iInfoSize - iCopy);
            memcpy(iShm, reinterpret_cast<uint8_t*>(pData) + iInfoSize - iCopy, iCopy);
            memcpy(iShm + iCopy, pData->mData, pData->mDataSize);
            iCopy += pData->mDataSize;
            *iWritePos = iCopy;
        }
    }

    // Mutex가 막고있는 다른 process 및 thread의 접근을 품
    pMtx->UnLock();

    return true;
}

inline int32_t ReadFromShm(uint8_t* pShm, const int32_t pBuffSize, SHM_NAL_DATA* pData, GWBASE::GWMutex* pMtx) {
    pMtx->Lock();

    uint32_t* iReadPos = reinterpret_cast<uint32_t*>(pShm);
    uint8_t* iShm = pShm + (sizeof(uint32_t) * 2);
    uint64_t iDataNum = pData->mDataNum;
    uint8_t* iData = nullptr;
    uint32_t iInfoSize = sizeof(SHM_NAL_DATA) - sizeof(pData->mData);
    uint64_t iCopy = 0;
    int32_t iRet = SHM_RET_TYPE::NO_ERROR;

    iCopy = *iReadPos + iInfoSize;
    if (pBuffSize > iCopy) {
        LOG_D("ReadFromShm() - case 1");
        memcpy(pData, iShm + *iReadPos, iInfoSize);
    } else {
        LOG_D("ReadFromShm() - case 2");
        iCopy = iCopy - pBuffSize;
        memcpy(reinterpret_cast<uint8_t*>(pData), iShm + *iReadPos, iInfoSize - iCopy);
        memcpy(reinterpret_cast<uint8_t*>(pData) + iInfoSize - iCopy, iShm, iCopy);
    }

    if (iDataNum == pData->mDataNum) {
        iRet = SHM_RET_TYPE::NO_ERROR;
    } else if (iDataNum > pData->mDataNum) {
        iRet = SHM_RET_TYPE::E_NOT_YET;
    } else {
        iRet = SHM_RET_TYPE::E_OVER;
    }

    if (iRet == SHM_RET_TYPE::NO_ERROR) {
        iData = reinterpret_cast<uint8_t*>(malloc(sizeof(uint8_t) * pData->mDataSize));

        if (pBuffSize > (iCopy + pData->mDataSize)) {
            memcpy(iData, iShm + iCopy, pData->mDataSize);
            iCopy += pData->mDataSize;
        } else {
            LOG_D("ReadFromShm() - case 3");
            iCopy = iCopy + pData->mDataSize - pBuffSize;
            memcpy(iData, iShm + *iReadPos + iInfoSize, pData->mDataSize - iCopy);
            memcpy(iData + pData->mDataSize - iCopy, iShm, iCopy);
        }
        pData->mData = iData;

        *iReadPos = iCopy;
    }

    pMtx->UnLock();

    return iRet;
}
